<?php
/* [STATIC/DYNAMIC] CONTENT PAGE */

require_once 'core/html/HTMLInfo.php';
require_once 'core/CommonFunctions.php';
require_once 'core/html/HTMLTemplator.php';

\HTML\Info::setKey('TITLE', 'Membership');

\HTML\Templator::getInstance()->add_template('templates/base_member.php');
\HTML\Templator::getInstance()->add_asset('css', 'member', \Common\Functions::linkAsset('css', 'member.css'));

\HTML\Templator::getInstance()->renderPage();
