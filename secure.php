<?php

require_once 'core/SessionHandler.php';

require_once 'core/CommonAccess.php';
require_once 'core/CommonFunctions.php';
require_once 'core/ErrorHandler.php';

set_exception_handler('\Core\ErrorHandler::exceptionHandler');

\Session\SessionHandler::start('_GTL_LOGIN');

if (!isset($_SESSION['loggedin'])) {
    header('Location: ' . "login.php");
    exit;
}

if (!isset($_GET['r'])) {
    header('Location: ' . "index.php");
    exit;
}

$uname = $_SESSION['username'];
if (!\Common\Access::is_user_admin()) {
    throw new Exception($uname . ' access denied');
}

$redirect_url = $_GET['r'];

/* Access granted */

$_SESSION['aauth'] = 1;
header('Location: ' . "$redirect_url");
exit;