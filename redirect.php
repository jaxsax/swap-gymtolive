<?php

/* Redirection via this page requires that a user have a valid account state */

require_once 'core/SessionHandler.php';

require_once 'core/CommonAccess.php';
require_once 'core/CommonFunctions.php';
require_once 'core/ErrorHandler.php';

set_exception_handler('\Core\ErrorHandler::exceptionHandler');

\Session\SessionHandler::start('_GTL_LOGIN');

if (!isset($_SESSION['loggedin'])) {
    header('Location: ' . "login.php");
    exit;
}

$s = \Core\db::get()->select(
    array('account_state'),
    'user',
    array('userid'),
    1
);

$s->bind_param('i', $_SESSION['uid']) or die;
$s->execute() or die;
$s->bind_result($state) or die;
$s->fetch() or die;

if ($state == \Common\Access::getAccountStates()['UNACTIVATED']) {
    header('Location: ' . "activate.php?a=react");
    exit;
}

$_SESSION['bauth'] = 1;

if (!isset($_GET['r'])) {
    header('Location: ' . "index.php");
    exit;
}

$redirect_url = $_GET['r'];
header('Location: ' . "$redirect_url");
exit;