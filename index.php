<?php
/* [STATIC/DYNAMIC] LOGIC/CONTENT PAGE */

require_once 'core/html/HTMLInfo.php';
require_once 'core/html/HTMLTemplator.php';

\HTML\Info::setKey('TITLE', 'Homepage');

\HTML\Templator::getInstance()->add_asset('css', 'index', \Common\Functions::linkAsset('css', 'index.css'));

\HTML\Templator::getInstance()->add_template('templates/base_index.php');
\HTML\Templator::getInstance()->renderPage();
