<?php
/* DYNAMIC LOGIC PAGE */

require_once 'core/CommonField.php';
require_once 'core/CommonFunctions.php';
require_once 'core/SessionHandler.php';
require_once 'core/Validation.php';
require_once 'core/dbhandler.php';

\Session\SessionHandler::start('_GTL_LOGIN');

if (!isset($_SESSION['loggedin'])) {
    die('{error: requires login}');
}

if (isset($_POST['comment']['submit'])) {

    $pid = \Common\Field::POSTValid('pid', 'comment');
    $uid = $_SESSION['uid'];
    $info = \Common\Field::POSTValid('data', 'comment');

    /* check for valid product */
    $product_validation = \Core\db::get()->select(
        array('product_id'),
        'product',
        array('product_id'),
        1
    );

    $product_validation->bind_param('s', $pid) or die;
    $product_validation->execute() or die;
    $product_validation->store_result() or die;
    if ($product_validation->num_rows <= 0)
        die('{error: invalid product id}');

    /* validate comment data */
    if (!\Common\Validation::isValidText($info)) {
        die('Please enter something more meaningful');
    }

    $info = \Common\Validation::cleanXSS($info);

    $c = \Core\db::get()->connect();
    $query = "INSERT INTO `product_comment`(`fk_product_id`, `fk_userid`, `comment`)
    VALUES(?,?,?)";
    $s = $c->prepare($query);
    if (!$s)
        die('prepare() failed');

    $s->bind_param('sss', $pid, $uid, $info) or die;
    $s->execute() or die;

    if ($s->affected_rows <= 0)
        die('product comment insert failed');

    header('Location: ' .
    "product.php?pid=$pid#$uid:$s->insert_id");
    exit;
} else if (!empty($_GET['a'])) {
    $action = $_GET['a'];
    switch($action) {
        case 'del':
            /* Delete comment if session uid matches database or if you are an administrator */

            $cid = \Common\Field::GETValid('cid');
            $s = \Core\db::get()->select(
                array('fk_userid', 'comment_id', 'fk_product_id'),
                'product_comment',
                array('comment_id'),
                1
            );

            $s->bind_param('s', $cid) or die;
            $s->execute() or die;
            $s->store_result() or die;

            if ($s->num_rows <= 0) {
                die('Invalid comment id');
            }

            $s->bind_result($fk_uid, $db_cid, $fk_pid) or die;
            $s->fetch() or die;

            $pid = $fk_pid;
            $uid = $fk_uid;
            $s->close();

            if (!\Common\Access::is_user_admin() && $_SESSION['uid'] != $uid)
                die("Access denied, you can not delete another user's comment");

            $c = \Core\db::get()->connect();
            $query = "DELETE FROM `product_comment` WHERE `comment_id` = ?";
            $dq = $c->prepare($query);
            if (!$dq)
                die('prepare() failed');

            $dq->bind_param('i', $cid) or die;
            $dq->execute() or die;
            if ($dq->affected_rows <= 0)
                die('comment delete failed');

            header('Location: ' . "product.php?pid=$pid");
            break;
        default:
            die('invalid action');
    }
}