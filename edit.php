<?php
/* DYNAMIC LOGIC PAGE */

require_once 'core/SessionHandler.php';
require_once 'core/CommonAccess.php';
require_once 'core/CommonField.php';
require_once 'core/Validation.php';
require_once 'core/dbhandler.php';

define('PRODUCT_IMAGES_FOLDER', 'product_images');

use \Common\Field;
use \Common\Validation;
\Session\SessionHandler::start('_GTL_LOGIN');

if (!\Common\Access::is_user_admin()) {
    die('{error: invalid user}');
}

function diverse_array($vector) {
    $result = array();
    foreach($vector as $key1 => $value1)
        foreach($value1 as $key2 => $value2)
            $result[$key2][$key1] = $value2;
    return $result;
}

/* check file name to see if a valid extension is found
    This removes any invalid extensions if it preserves a valid extension
    IE: a.php.jpg => a.jpg
        a.php => null (invalid)

    RETURN: null
    RETURN: [random 5char].[valid extension]
*/
function validate_extension($ext, $ext_arr) {

    foreach($ext_arr as $valid_ext) {
        if ($valid_ext == $ext)
            return $valid_ext;
    }
    return null;
}

/* check if file is uploaded via POST */
function upload_valid($tmp_name) {

    if (!empty($tmp_name) && is_uploaded_file($tmp_name))
        return $tmp_name;

    return null;
}

/* check tmp_name to see if
    1) it is uploaded via HTTP POST
    2) has a valid mimetype via getimagesize

    RETURN: null if (is not uploaded via POST or getimagesize returns false)
*/
function validate_image($img_path, $mtype_arr) {

    if (($tmp_name = upload_valid($img_path)) == null)
        return null;

    $image = getimagesize($tmp_name);
    if (!$image)
        return null;

    foreach($mtype_arr as $mimetype) {
        if ($image['mime'] == $mimetype)
            return $img_path;
    }

    return null;
}

function validate_size($size) {
    $MAX_SIZE = 350 * 1024 * 8; /* 350KB */
    $size_in_kB = ($size / 1024.0) / 8.0;

    if ($size_in_kB > $MAX_SIZE)
        return -1;
    return $size;
}

function validate_array($arr) {

    $valid_ext = array('jpg', 'jpeg', 'png');
    $valid_mtype = array('image/jpg', 'image/jpeg', 'image/png');

    $new_arr = array();

    /* FYI: mimetype sent from client is ignored */
    foreach($arr as $value) {
        /* check extensions, check if ending extension is recoverable */

        if (empty($value['tmp_name']))
            continue;

        $intern_array = array();
        $intern_array['size'] = validate_size($value['size']); /* -1 if error */

        $image = explode('.', $value['name']);
        $intern_array['name'] = \Common\Security::generateRandChar(5);
        $intern_array['ext'] = $image[1];

        // $intern_array['name'] = validate_extension($value['name'], $valid_ext); /* null if error */
        $intern_array['tmp_name'] = validate_image($value['tmp_name'], $valid_mtype); /* null if error */

        $new_arr[] = $intern_array;
    }

    return $new_arr;
}

if (isset($_POST['product']['info-submit'])) {
    /* mandatory fields: name, type, brand, price */
    $pid = Field::POSTValid('id', 'product');
    $name = Field::POSTValid('name', 'product');
    $type = Field::POSTValid('type', 'product');
    $brand = Field::POSTValid('brand', 'product');
    $price = Field::POSTValid('price', 'product');

    /* optional fields */
    $summary = urlencode(Field::POSTValid('summary', 'product'));

    $mandatory_fields = array($name, $type, $brand);
    $error_fields = \Common\Field::valid_field_length(30, $mandatory_fields);

    if (!empty($ea)) {
        foreach($ea as $error_index) {
            echo "error field: $error_index, field length exceeded<br />";
        }
        die;
    }

    if (!preg_match('/^[0-9]{1,3}(\.[0-9]{2})?$/', $price)) {
        die('price not properly formatted got[' . $price . '] expected DDD:CC');
    }

    /* validate string fields */
    if (!Validation::validateFieldList('\Common\Validation::isStringField', $mandatory_fields))
        die('fields failed input validation');

    $query = "UPDATE `product` SET
    `product_name` = ?,
    `product_type` = ?,
    `product_brand` = ?,
    `product_summary` = ?,
    `unit_price` = ?
    WHERE `product_id` = ?";

    $con = \Core\db::get()->connect();
    $stmt = $con->prepare($query);
    if (!$stmt)
        die('prepare() failed');

    $stmt->bind_param('ssssds', $name, $type, $brand, $summary, $price, $pid) or die;
    $stmt->execute() or die;
    if ($stmt->affected_rows <= 0)
        die('update product failed' . $stmt->error);

    header('Location: ' . "product.php?pid=$pid");
    exit;

} else if (isset($_POST['product']['image-submit'])) {

    /* perform deletion first */
    if (!empty($_POST['image-cb'])) {
        /* generate a map of product id => image id */

        $generic_path = $_SERVER['DOCUMENT_ROOT'] . '/' . \Common\Functions::$INFO_TABLE['FOLDER_URL'] .
            '/static' .
            '/' . PRODUCT_IMAGES_FOLDER;

        foreach($_POST['image-cb'] as $product_string) {

            var_dump($product_string);

            /* invalid format; product id:image id:extension */
            if (substr_count($product_string, ':') < 1)
                continue;

            # 0 - product id
            # 1 - image id
            $mapping = explode(':', $product_string);

            $stmt = \Core\db::get()->select(
                array('fk_product_id', 'image_id', 'image_ext'),
                'product_images',
                array('fk_product_id', 'image_id'),
                1
            );

            $stmt->bind_param('ss', $mapping[0], $mapping[1]) or die;
            $stmt->execute() or die;
            $stmt->store_result() or die;
            if ($stmt->num_rows <= 0) {
                die('invalid identifier');
            }

            $stmt->bind_result($pid, $id, $ext) or die;
            $stmt->fetch() or die;
            $stmt->close();

            $path = $generic_path . '/' . $pid . '/' . "$id.$ext";

            $query = "DELETE FROM `product_images` WHERE `fk_product_id` = ? AND `image_id` = ?";
            $c = \Core\db::get()->connect();

            $stmt = $c->prepare($query);
            if (!$stmt)
                die('prepare() failed');

            $stmt->bind_param('ss', $pid, $id) or die;
            $stmt->execute() or die;
            if ($stmt->affected_rows <= 0) {
                die('db deletion failed');
            }

            if (!unlink($path))
                die('image deletion failed');
        }
    }

    $pid = \Common\Field::POSTValid('id', 'product');
    if ($pid == null)
        die('unspecified product id');

    /* upload images */
    $ordered_array = diverse_array($_FILES['product-images']);
    $ordered_array = validate_array($ordered_array);

    /* insert images */
    $product_image_query = "INSERT INTO `product_images`(
    `fk_product_id`, `image_id`, `image_ext`)
    VALUES(?,?,?)";

    $con = \Core\db::get()->connect();
    $stmt = $con->prepare($product_image_query);
    if (!$stmt) {
        die('Database error');
    }

    $generic_path = \Common\Functions::$INFO_TABLE['FOLDER_URL'] .
        '/static' .
        '/' . PRODUCT_IMAGES_FOLDER;

    $image_path_dir = $_SERVER['DOCUMENT_ROOT'] . '/' . $generic_path;

    foreach($ordered_array as $value) {

        if ($value['size'] == -1) {
            die('Invalid image size, max 350kB');
        }

        if ($value['name'] == null) {
            die('invalid file extension, was not recoverable');
        }

        if ($value['tmp_name'] == null) {
            die('invalid image, only jpeg and png is allowed');
        }

        //echo $value['name'] . '<br />';

        $img_disk_path_dir = $image_path_dir . '/' . $pid;
        if (!is_dir($img_disk_path_dir))
            mkdir($img_disk_path_dir, 0, true);

        $img_disk_path = $img_disk_path_dir . '/' . $value['name'] . '.' . $value['ext'];

        move_uploaded_file($value['tmp_name'], $img_disk_path);
        $stmt->bind_param('sss',
            $pid, $value['name'], $value['ext']) or die('Database error');

        $stmt->execute() or die('Database error' . $con->error);
        if ($stmt->affected_rows <= 0) {
            die('Image ' . $pid . ':' . $value['name'] . ' was not inserted ' . $con->error);
        }
    }
    header('Location: ' . "product.php?pid=$pid");
    exit;
}