<?php

require_once 'core/dbhandler.php';

$expr = '/[\<\>\"\'\%\;\(\)\&\+]/';
$str = array("Super Amino 6000", 'Amino Acid Pills', "Dymatize");

$c = \Core\db::get()->connect();
for ($i = 0; $i < count($str); $i++)  {

    $temp = htmlspecialchars($str[$i]);

    echo "<p>$temp</p>\n";
    $str[$i] = $temp;
}

$res = TFL_valid_re($str, $expr);
echo $res ? 'true' : 'false';

function TFL_valid_re($list, $expr) {
    foreach($list as $value) {
        if (preg_match($expr, $value))
            return false;
    }
    return true;
}