<!doctype html>
<html>
<head>
    <title>How to utilize database</title>

    <style>
        .id {
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;

            background: rgb(255, 221, 118);
        }
        .type {
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;

            background: rgb(203, 255, 120);
        }
        .ninfo {
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;

            background: rgb(52, 255, 58);
        }
        .summary {
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;

            background: rgb(67, 186, 255);
        }

    </style>
</head>

<body>
<?php

/* Before using, it must be included in */
require_once ('core/dbhandler.php');

/* To use database, get a connection using */
$c = \Core\db::get()->connect();
if ($c) {
    /* Execution code
        This is an example of a basic SELECT statement
       There are two ways of doing this
    */

    // Firstly, you can utilize the connection [$c] and do it like how everybody does it
    // Make sure to encapsulate it in if's for error handling

    if ($stmt = ($c->prepare("SELECT * FROM `product`"))) { /* prepares the statement for use */
        if ($stmt->execute()) { /* executes statement */

            /* This is IMPORTANT, if you want to get
                number of rows returned, you must store it first
            */
            $stmt->store_result();
            echo "Number of products: $stmt->num_rows\n";

            /* next, bind results to our variables -- BIND BEFORE FETCH */
            $stmt->bind_result($pid, $pname, $ptype, $pbrand, $pninfo, $psummary);
            /* Since we are pulling the whole table our, we can use a while loop to help
                us populate data
            */

            while($stmt->fetch()) {
                echo "<div class='id'>$pid $pname</div>\n";
                echo "<div class='type'>$ptype - $pbrand</div>\n";
                echo "<div class='ninfo'>$pninfo</div>\n";
                echo "<div class='summary'>$psummary</div>\n";
            }
        }
    }

    /* Always close the statement as well */
    $stmt->close();
}

/* This second way handles connections and preparations for you
    You can only use SELECT statements here

    Heres an example on how to use binds on the second method
*/

$product_id = '5efgh'; /* pre set value */
$stmt = \Core\db::get()->squery("SELECT * FROM `product` WHERE `product_id` = ?");
if ($stmt) {
    if ($stmt->bind_param('s', $product_id)) {
        if ($stmt->execute()) {

            /* Once again, num_rows works here only if you store the results */
            $stmt->store_result();
            echo "Number of products: $stmt->num_rows\n";
            $stmt->bind_result($pid, $pname, $ptype, $pbrand, $pninfo, $psummary);

            while($stmt->fetch()) {
                echo "<div class='id'>$pid $pname</div>\n";
                echo "<div class='type'>$ptype - $pbrand</div>\n";
                echo "<div class='ninfo'>$pninfo</div>\n";
                echo "<div class='summary'>$psummary</div>\n";
            }
        }
    }
    $stmt->close();
}

/* Always make sure to close the connection after you are done. */
\Core\db::get()->close_con();

/* This is basically, how to use the database handler
    So you decide what method you want to use
*/
?>

</body>
</html>