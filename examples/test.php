<!doctype html>
    <html>

<head>
    <title>Form Test</title>

    <style>
        fieldset label {
            display: inline-block;
        }
        fieldset input {
            display: block;
        }
    </style>
</head>

<body>

<form action='#' method='get'>

    <fieldset class='personal'>
        <legend>Personal Information</legend>
        <div class='field-list'>
            <label for='first_name'>
                First Name
                <input name='first_name' size='30' type='text' />
            </label>

            <label for='last_name'>
                Last Name
                <input name='last_name' size='30' type='text' />
            </label>
        </div>
        <div class='field-list'>
            <label for='user_name'>
                Username
                <input name='user_name' size='30' type='text' />
            </label>
            <label for='birth_date'>
                <input name='birth_date' type='date' />
            </label>
        </div>
    </fieldset>

</form>
</body>
    </html>