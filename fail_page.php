<?php
/* DYNAMIC CONTENT PAGE */

require_once 'core/html/HTMLInfo.php';
require_once 'core/html/HTMLTemplator.php';

\HTML\Info::setKey('TITLE', 'Profile modification error');
\HTML\Templator::getInstance()->add_template('templates/base_fail_page.php');
\HTML\Templator::getInstance()->renderPage();