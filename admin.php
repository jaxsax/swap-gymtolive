<?php
/* DYNAMIC LOGIC/CONTENT PAGE */

$SECURE_PAGE = 1;
include_once 'core/https_config.php';

require_once 'core/SessionHandler.php';
require_once 'core/CommonField.php';
require_once 'core/dbhandler.php';

\Session\SessionHandler::start('_GTL_LOGIN');
if (!isset($_SESSION['aauth'])) {
    header('Location: /gtl/secure.php?r=admin.php');
    exit;
}

$uid = $_SESSION['uid'];
$uname = $_SESSION['username'];

?>
<!doctype html>
<html lang='en'>
<head>
    <meta charset='utf-8'>
    <title>Administration</title>
    <link rel='stylesheet' href='<?=\Common\Functions::linkAsset('css', 'form-common.css')?>' />
    <link rel='stylesheet' href='<?=\Common\Functions::linkAsset('css', 'admin.css')?>' />
</head>
<body>

<div id='admin-informer'>
    <h1 class='nav-text'>
        Administration panel
    </h1>

    <h1 class='nav-text'>
        Welcome, <?=$uname?>
    </h1>
</div>
<nav>
    <ul>
        <li>
            <a href='index.php'>Home</a>
        </li>
        <li>
            <a href='admin.php?mode=user'>User Administration</a>
        </li>
        <li>
            <a href='add.php?a=product'>Add product</a>
        </li>
    </ul>
</nav>

<div id='admin-content-wrapper'>
<?php

$mode = 'none';
if (isset($_GET['mode'])) {
    $mode = $_GET['mode'];
}

switch($mode) {
    case 'user':
        ?>
            <fieldset>
                <legend>User administration</legend>
                <form action='admin.php?mode=user' method='post'>
                    <div class='field-list'>
                        <label for='query[user]'>
                            <input name='query[user]' placeholder='User search query'/>
                        </label>
                    </div>
                    <div class='field-list'>
                        <input name='query[submit]' type='submit' value='Search' />
                    </div>
                </form>

                <?php

                $c = \Core\db::get()->connect();

                if (isset($_POST['query']['submit']) && !empty($_POST['query']['user'])) {

                    $q = $_POST['query']['user'];
                    $q = $c->real_escape_string($q);

                    $query = "SELECT `userid`, `user_name`, `account_state`, `date_joined`, `last_expired` FROM `user`
                    WHERE CONCAT(`user_name`, ' ', `first_name`, ' ', `last_name`) LIKE '%" . $q . "%'";

                    $s = $c->prepare($query);
                    if (!$s)
                        die('prepare() failed');
                } else {
                    $s = \Core\db::get()->select(
                        array('userid', 'user_name', 'account_state', 'date_joined', 'last_expired'),
                        'user'
                    );
                }

                $s->execute() or die;
                $s->store_result() or die;
                if ($s->num_rows <= 0) {
                    echo 'No users found';
                } else {
                    $s->bind_result($uid, $uname, $state, $dj, $le) or die;
                    ?>
                    <table>
                        <tr>
                            <td>ID</td>
                            <td>Username</td>
                            <td>Account state</td>
                            <td>Date joined</td>
                            <td>Last expired</td>
                        </tr>
                        <?php
                    while($s->fetch()) {

                        $state = $state == \Common\Access::getAccountStates()['UNACTIVATED']? 'DEACTIVATED' : 'ACTIVATED';
                    ?>
                        <tr>
                            <td>
                                <?=$uid?>
                            </td>
                            <td>
                                <a href='manage_profile.php?uid=<?=$uid?>'>
                                    <?=$uname?>
                                </a>
                            </td>
                            <td>
                                <?=$state?>
                            </td>
                            <td>
                                <?=$dj?>
                            </td>
                            <td>
                                <?=$le?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </table>
                <?php
                }
                ?>
            </fieldset>
        <?php
        break;
}
?>
</div>

</body>
</html>