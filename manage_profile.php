<?php

$SECURE_PAGE = 1;
include_once 'core/https_config.php';
/* DYNAMIC LOGIC/CONTENT PAGE */

require_once 'core/html/HTMLInfo.php';
require_once 'core/html/HTMLTemplator.php';

\HTML\Info::setKey('TITLE', 'Homepage');

\HTML\Templator::getInstance()->add_asset('css', 'form-common', \Common\Functions::linkAsset('css', 'form-common.css'));
\HTML\Templator::getInstance()->add_template('templates/base_manage_profile.php');
\HTML\Templator::getInstance()->renderPage();

