<?php

namespace Common;

require_once 'core/CommonAccess.php';
use \Common\Access;

final class Functions {

    private function __construct() {}

    public static $INFO_TABLE = array(
        "FOLDER_URL"		=> "gtl",
        "ASSET_URL"			=> "gtl/static",
    );

    public static $URL_TABLE = array(

        'index.php'		        => "HOME",
        'workouts.php'	        => "WORKOUTS",
        'product.php?page=1'	=> "PRODUCTS",
        'about.php'	            => "ABOUT US",
        'member.php'	        => "MEMBERSHIP",
    );

    public static function getURLKeys() {
        return array_keys(self::$URL_TABLE);
    }

    public static function getURLValue($key) {
        return self::$URL_TABLE[$key];
    }

    public static function getURLTable() {
        return self::getURLTable();
    }

    /* returns a value from INFO_TABLE identified by key if it exists */
    public static function getInfoTableValue($key) {
        return isset(self::$INFO_TABLE[$key]) ? self::$INFO_TABLE[$key] : '';
    }

    /* generate a URL according to address */
    private static function gen_url($key) {
        $url = 'http://';
        if (isset($_SERVER['HTTP_HOST'])) {
            $url .= $_SERVER['HTTP_HOST'] . '/';
        }
        $url .= self::getInfoTableValue($key) . '/';
        return $url;
    }

    /* $content_folder -- folder name relative to static/
        E.G: static/css/ -> css */
    public static function linkAsset($content_folder, $content_name) {
        return '/' . self::$INFO_TABLE['ASSET_URL'] . "/$content_folder/$content_name";
    }
}
