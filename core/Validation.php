<?php
/* Common terms

StringField: alphanumeric
NumberField: numeric
*/

namespace Common;


class Validation {

    /* StringField
    ACCEPT[character,number] */
    public static function isStringField($s) {
        return preg_match('/^[\w ]*$/i', $s);
    }

    /* NumberField
    ACCEPT[number] */
    public static function isNumberField($s) {
        return preg_match('/^[\d]*$/', $s);
    }

    /*
     * more methods may be added in the future
     *
     */
    public static function getCleanString($s) {
        str_replace("\r\n", "\n", $s);
        $s = trim($s);

        return $s;
    }

    public static function validateFieldList($function, array $input_list) {
        foreach ($input_list as $value) {
            if (!call_user_func($function, $value))
                return false;
        }
        return true;
    }

    public static function cleanXSS($input) {
        $n = str_replace('"', "'", $input);
        $n = strip_tags($input);
        $n = htmlspecialchars($n);

        return $n;
    }

    /* May have additional checks in the future */
    public static function isValidText($input) {
        return strlen($input) > 0;
    }

    public static function CSRFToken_generate() {
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        $sess_id = isset($_SESSION) ? session_id() : '';

        $nonce = base64_encode($referer . $sess_id);
        if (empty($_SESSION['csrf_tokens'])) {
            $_SESSION['csrf_tokens'] = array();
        }
        $_SESSION['csrf_tokens'][$nonce] = true;
        return $nonce;
    }

    public static function CSRFToken_validate($token) {
        if (isset($_SESSION['csrf_tokens'][$token])) {
            unset($_SESSION['csrf_tokens'][$token]);
            return true;
        }
        return false;
    }
}