<?php

namespace Common;
final class Security {

    private function __construct() {}

    private static $SALT_CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    /* SHA512 default length: 16
       Generate random characters up to x length
    */
    public static function generateRandChar($length = 16) {

        $salt = '';
        for ($i = 0; $i < $length; ++$i)
            $salt .= self::$SALT_CHARS[rand(0, strlen(self::$SALT_CHARS) - 1)];

        return $salt;
    }

    /* generate a SHA512 password
    SHA-512 hash with a sixteen character salt prefixed with $6$. If the salt string starts with 'rounds=<N>$',
    the numeric value of N is used to indicate how many times the hashing loop should be executed
    */
    public static function generatePassword($input, $salt, $rounds = 5000) {
        return crypt($input, sprintf('$6$rounds=%d$', $rounds) . $salt);
    }

    /* SlowEquals on a password to prevent timing attacks
        http://crackstation.net/hashing-security.htm#salt
    */
    public static function slowEq($s0, $s1) {

        $d = strlen($s0) ^ strlen($s1); /* any value that is xor'd with itself is 0 */
        for ($i = 0; $i < strlen($s0); ++$i)
            /* set bit to 1 if they are not the same  */
            $d |= ord($s0[$i]) ^ ord($s1[$i]);

        return $d == 0;
    }
}

