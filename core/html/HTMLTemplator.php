<?php
	
namespace HTML;

require_once 'core/CommonFunctions.php';
final class Templator {

    const TEMPLATE_START_PATH = 'templates/base_html_start.php';
    const TEMPLATE_END_PATH = 'templates/base_html_end.php';
	
	private $TEMPLATE_LIST = array();
	private $ASSET_TABLE = array();

    private function __construct() {
        /* common assets to be added to every page */
        $this->add_asset('css', 'reset', \Common\Functions::linkAsset('css', 'reset-min.css'));
        $this->add_asset('css', 'base', \Common\Functions::linkAsset('css', 'base.css'));
    }

	
	public static function getInstance() {
		static $INSTANCE = null;
		if ($INSTANCE == null)
			$INSTANCE = new Templator();
	
		return $INSTANCE;
	}
	
    /* output generator */
	public function renderPage() {

		include_once(\HTML\Templator::TEMPLATE_START_PATH);
		foreach ($this->TEMPLATE_LIST as $value)
			include_once($value);
		include_once(\HTML\Templator::TEMPLATE_END_PATH);
	}
	
    /* adds a template to the existing template list */
	public function add_template($path) {
		array_push($this->TEMPLATE_LIST, $path);
	}

    public function add_asset($format, $key, $url) {
        $this->ASSET_TABLE[$format][$key] = $url;
    }

    public function link_css() {
        foreach($this->ASSET_TABLE['css'] as $url) {
            echo "<link rel='stylesheet' type='text/css' href='" . $url  . "' />\n";
        }
    }
}