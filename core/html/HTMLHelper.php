<?php

namespace HTML;

/* test for valid text field
    T: valid
    F: invalid
*/
function TF_valid($field_value) {
    if (is_string($field_value))
        return strlen($field_value) > 0;
    return false;
}

/* test a list of fields for validity
    T: valid
    F: invalid
*/
function TFL_valid($list) {
    foreach($list as $value) {
        if (!TF_valid($value))
            return false;
    }
    return true;
}

/* test a list of fields according to regular expression $expr
    T: valid
    F: invalid
*/
function TFL_valid_re($list, $expr) {
    foreach($list as $value) {
        if (preg_match($expr, $value))
            return false;
    }
    return true;
}

/* text field creator */
function TF_create($name, $pname, $value) {
    echo "<tr>\n";
    echo "<td>$pname</td>\n";
    echo "<td><input type='text' name='$name' value='$value'/>\n";
    echo "</tr>\n";
}

?>