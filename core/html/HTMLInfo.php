<?php

namespace HTML;

final class Info {

    protected static $HTML_INFO_TABLE = array();

    public static function getValue($key) {
        if (isset(\HTML\Info::$HTML_INFO_TABLE[$key]))
            return \HTML\Info::$HTML_INFO_TABLE[$key];
        return '';
    }

	public static function setKey($key, $value) {
		\HTML\Info::$HTML_INFO_TABLE[$key] = $value;
	}
}

?>