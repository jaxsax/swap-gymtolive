<?php
	
namespace Core;

define('DB_HOST', '127.0.0.1');
define('DB_USER', 'root');
define('DB_PASSWORD', '');
define('USE_DATABASE', 'gtl');

final class db {

    protected static $instance = null;
    private $db_con = null;
	
	public static function get() {

        if (is_null(self::$instance))
            self::$instance = new db();
        return self::$instance;
	}

    /* prevent instance creation */
    private function __construct() {}
    private function __clone() {}

	public function connect($db = null) {

        if (is_null($this->db_con)) {
            $this->db_con = new \mysqli(DB_HOST, DB_USER, DB_PASSWORD, is_null($db) ? USE_DATABASE : $db);
            if ($this->db_con->connect_errno)
                die('failed connection to database');
        }

        return $this->db_con;
	}

    /* doesn't really need to be closed */
	public function close_con() {

        if (!is_null($this->db_con)) {
            $this->db_con->close();
            $this->db_con = null;
        }
	}

    public function squery($select_query) {

        if (!preg_match('/^SELECT(.*)/i', $select_query)) {
            return null;
        }

        $c = $this->connect();
        if (!$c) {
            return null;
        }

        $stmt = $c->prepare($select_query);
        if (!$stmt) {
            return null;
        }

        return $stmt;
    }

    public function select(array $fields = null, $table = null,
                           array $conditions = null,
                           $limit = 0)
    {

        if (!$table or strlen($table) <= 0) {
            die('invalid table argument');
        }

        $db_query = 'SELECT ';
        $field_it = new \CachingIterator(new \ArrayIterator($fields));

        /* generate fields */
        foreach ($field_it as $field) {
            $db_query .= $field;
            if ($field_it->hasNext())
                $db_query .= ',';
            $db_query .= ' ';
        }
        $db_query .= 'FROM ' . $table . ' ';

        /* generate conditionals */
        if ($conditions) {
            $conditions_length = count($conditions);
            $first = true;

            for ($i = 0; $i < $conditions_length; $i++) {
                if ($first) {
                    $db_query .= 'WHERE ';
                    $first = false;
                } else {
                    $db_query .= ' AND ';
                }
                $db_query .= $conditions[$i] . ' = ?';
            }
        }

        if ($limit > 0) {
            $db_query .= " LIMIT $limit";
        }

        $stmt = $this->connect()->prepare($db_query);
        if (!$stmt)
            die;

        return $stmt;
    }
}
?>