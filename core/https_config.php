<?php

/* http://www.ideologics.co.uk/programming/how-to-use-https-effectively-in-your-php-website */

/* if SECURED_PAGE is undefined, we assume an unsecured page is requested */
if (isset($SECURE_PAGE) && $SECURE_PAGE) {
    $_SECURED = $SECURE_PAGE;
} else {
    $_SECURED = 0;
}

if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') {

    if (!$_SECURED) {
        // but we shouldn't be!
        $url="http://localhost" . $_SERVER['REQUEST_URI'];

        header('Location: '.$url);
        exit;
    }
} else {
    // we aren't on a secure page.
    if ($_SECURED) {
        // but we should be!

        $url="https://localhost" . $_SERVER['REQUEST_URI'];
        header('Location: '.$url);
        exit;
    }
}
