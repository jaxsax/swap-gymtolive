<?php

namespace Common;
final class Field {

    private static function _validate_field($v) {

        if (strlen($v) <= 0 || is_null($v))
            return null;

        $value = urldecode($v);
        $value = str_replace('\0', '', $value); // prevent nul byte

        return $value;
    }

    public static function has_field($method, $key) {
        $method = strtolower($method);

        switch($method) {
            case 'get':
                return isset($_GET[$key]) && strlen($_GET[$key]) > 0;
                break;
            case 'post':
                return isset($_POST[$key]) && strlen($_POST[$key]) > 0;

            default:
                return '';
        }
    }

    public static function GETValid($key, $ns = null) {
        if (strlen($key) <= 0)
            return null;

        if ($ns != null)
            return isset($_GET[$ns][$key]) ? self::_validate_field($_GET[$ns][$key]) : null;
        return isset($_GET[$key]) ? self::_validate_field($_GET[$key]) : null;
    }

    public static function POSTValid($key, $ns = null) {
        if (strlen($key) <= 0)
            return null;
        if ($ns != null)
            return isset($_POST[$ns][$key]) ? self::_validate_field($_POST[$ns][$key]) : null;
        return isset($_POST[$key]) ? self::_validate_field($_POST[$key]) : null;
    }

    /* return an array of indexes that errored */
    public static function valid_field_length($length, $array) {

        $error_indexes = array();
        for($i = 0; $i < count($array); ++$i) {

            if (is_null($array[$i])) {
                array_push($error_indexes, $i);
                continue;
            }

            $vstrlen = strlen($array[$i]);
            if ($vstrlen <= 0) {
                array_push($error_indexes, $i);
                continue;
            }

            if ($vstrlen > $length) {
                array_push($error_indexes, $i);
                continue;
            }
        }

        return $error_indexes;
    }
}