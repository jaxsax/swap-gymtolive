<?php

namespace Common;

use Core\db;
final class Access {

    private function __construct() {}

    private static $USER_LEVELS = array(
        'INVALID' => -1,
        'CUSTOMER' => 0,

        'ADMIN_BASIC' => 10,
        'ADMIN_ADV' => 20,
        'ADMIN_FULL' => 100
    );

    private static $ACCOUNT_STATE = array(
        'UNACTIVATED' => 0,
        'ACTIVATED' => 1
    );

    const ACCOUNT_UNACTIVATED = 0;
    const ACCOUNT_ACTIVATED = 1;

    public static function is_user_admin() {

        if (!isset($_SESSION['loggedin']))
            return false;

        $uid = $_SESSION['uid'];
        $q = "SELECT COUNT(`fk_userid`) FROM `admin` WHERE `fk_userid` = ? LIMIT 1";
        $stmt = \Core\db::get()->squery($q);
        if (!$stmt) die('Database error');

        $stmt->bind_param('s', $uid) or die('Database error');
        $stmt->execute() or die('Database error');
        $stmt->bind_result($count) or die('Database error');
        $stmt->fetch() or die('Database error');

        if ($count <= 0)
            return false;

        return true;
    }

    public static function getUserLevels() {
        return self::$USER_LEVELS;
    }

    public static function getAccountStates() {
        return self::$ACCOUNT_STATE;
    }

    public static function getUserAccess($query_uid = null) {

        if (!isset($_SESSION['loggedin']))
            return self::$USER_LEVELS['INVALID'];

        $uid = ($query_uid == null ? $_SESSION['uid'] : $query_uid);
        if ($uid == null)
            return Access::getUserLevels()['INVALID'];

        $s = \Core\db::get()->select(
            array('access_level'),
            'user',
            array('userid'),
            1
        );

        $s->bind_param('i', $uid) or die;
        $s->execute() or die;
        $s->bind_result($access) or die;
        $s->fetch() or die;

        return $access;
    }

    public static function getUserState($query_uid = null) {
        if (!isset($_SESSION['loggedin']))
            return self::$ACCOUNT_STATE['UNACTIVATED'];

        $uid = ($query_uid == null ? $_SESSION['uid'] : $query_uid);
        if ($uid == null)
            return self::$ACCOUNT_STATE['UNACTIVATED'];

        $s = \Core\db::get()->select(
            array('account_state'),
            'user',
            array('userid'),
            1
        );

        $s->bind_param('i', $uid) or die;
        $s->execute() or die;
        $s->bind_result($state) or die;
        $s->fetch() or die;

        return $state;
    }
}