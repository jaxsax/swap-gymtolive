<?php

namespace Session;

require_once 'core/dbhandler.php';
require_once 'core/CommonFunctions.php';
require_once 'core/CommonSecurity.php';

use \Core\db;
use \Common\Security;
use \Common\Functions;

/* http://blog.teamtreehouse.com/how-to-create-bulletproof-sessions */
final class SessionHandler {

    static function start($name, $limit = 0, $path = '/', $domain = null, $secure = null) {

        session_name($name . '_SESS');
        $https = isset($secure) ? $secure : isset($_SERVER['HTTPS']);

        session_set_cookie_params($limit, $path, $domain, $https, true);
        if (session_status() !== PHP_SESSION_ACTIVE) {session_start();}

        if(self::validateSession())
        {
            if(!self::preventHijacking())
            {
                $_SESSION = array();
                $_SESSION['IPaddress'] = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
                $_SESSION['userAgent'] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
                self::regenerateSession();
            }
        } else {
            $_SESSION = array();
            session_destroy();
            session_start();
        }
    }

    public static function destroy() {

        $_SESSION = array();

        $param = session_get_cookie_params();
        setcookie(session_name(), '', time() - 10, $param['path'], $param['domain'], $param['secure'], $param['httponly']);
        session_unset();
        session_destroy();

        header('Location: ' . "index.php");
        exit;
    }

    static protected function preventHijacking() {
        if(!isset($_SESSION['IPaddress']) || !isset($_SESSION['userAgent']))
            return false;

        if ($_SESSION['IPaddress'] != $_SERVER['REMOTE_ADDR'])
            return false;

        if( $_SESSION['userAgent'] != $_SERVER['HTTP_USER_AGENT'])
            return false;

        return true;
    }

    static protected function validateSession()
    {
        if( isset($_SESSION['OBSOLETE']) && !isset($_SESSION['EXPIRES']) )
            return false;

        if(isset($_SESSION['EXPIRES']) && $_SESSION['EXPIRES'] < time())
            return false;

        return true;
    }

    public static function checkLogin($username, $password) {

        if (strlen($username) == 0 || strlen($password) == 0)
            return false;

        $stmt = db::get()->select(
            array('userid', 'password', 'salt', 'access_level'),
            'user',
            array('user_name'),
            1
        );

        $stmt->bind_param('s', $username) or die;
        $stmt->execute() or die;
        $stmt->store_result() or die;
        if ($stmt->num_rows <= 0) {
            return false;
        }

        $stmt->bind_result($udbid, $udbpass, $udbsalt, $access_level) or die;
        $stmt->fetch() or die;

        $provided_password = Security::generatePassword($password, $udbsalt);
        if (Security::slowEq($udbpass, $provided_password)) {

            $_SESSION['loggedin'] = true;
            $_SESSION['uid'] = $udbid;
            $_SESSION['username'] = $username;
            $_SESSION['level'] = $access_level;

            if ($access_level >= \Common\Access::getUserLevels()['ADMIN_BASIC'])
                $_SESSION['aauth'] = 1;
            var_dump($_SESSION['aauth']);
            return true;
        }

        $stmt->close();
        return false;
    }

    static function regenerateSession() {

        // If this session is obsolete it means there already is a new id
        if (isset($_SESSION['OBSELETE']))
            return;

        $_SESSION['OBSOLETE'] = true;
        $_SESSION['EXPIRES'] = time() + 10;

        // Create new session without destroying the old one
        session_regenerate_id(false);

        // Grab current session ID and close both sessions to allow other scripts to use them
        $newSession = session_id();
        session_write_close();

        // Set session ID to the new one, and start it back up again
        session_id($newSession);
        session_start();

        unset($_SESSION['OBSOLETE']);
        unset($_SESSION['EXPIRES']);
    }
}