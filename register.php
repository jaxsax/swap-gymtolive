<?php
/* STATIC CONTENT PAGE */

require_once 'core/html/HTMLInfo.php';
require_once 'core/html/HTMLTemplator.php';

\HTML\Info::setKey('TITLE', 'Setup your account');

\HTML\Templator::getInstance()->add_asset('css', 'form-common', \Common\Functions::linkAsset('css', 'form-common.css'));
\HTML\Templator::getInstance()->add_template('templates/base_register.php');
\HTML\Templator::getInstance()->renderPage();
