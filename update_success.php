<?php
/* STATIC CONTENT PAGE */

require_once 'core/html/HTMLInfo.php';
require_once 'core/html/HTMLTemplator.php';

\HTML\Info::setKey('TITLE', 'Update Successful');
\HTML\Templator::getInstance()->add_template('templates/base_update_success.php');
\HTML\Templator::getInstance()->renderPage();
