<?php
/* DYNAMIC LOGIC/CONTENT PAGE */

require_once 'core/html/HTMLInfo.php';
require_once 'core/html/HTMLTemplator.php';

$action = isset($_GET['a']) ? 'Add ' . ucfirst($_GET['a']) : '';

\HTML\Info::setKey('TITLE', $action);

\HTML\Templator::getInstance()->add_asset('css', 'form-common', \Common\Functions::linkAsset('css', 'form-common.css'));

\HTML\Templator::getInstance()->add_template('templates/base_add.php');
\HTML\Templator::getInstance()->renderPage();
