<?php

require_once 'core/dbhandler.php';
require_once 'core/SessionHandler.php';
require_once 'core/CommonAccess.php';

include_once 'core/CommonFunctions.php';
include_once 'core/CommonField.php';

$ITEMS_PER_PAGE = 3;

if (isset($_GET['product-query-submit'])) {

    if (!isset($_GET['q'])) {
        die('unspecified query parameter');
    }

    if (strlen($_GET['q']) <= 0) {
        header('Location: ' . "product.php");
        exit;
    }

    $question = urldecode($_GET['q']);
    $c = \Core\db::get()->connect();

    $q = "SELECT
    `product_id`, `product_name`,`product_type`, `product_brand`,`unit_price`
    FROM `product`
    WHERE CONCAT(`product_type`, ' ', `product_name`,' ',`product_brand`)
    LIKE '%" . $c->real_escape_string($question) . "%'";

    //var_dump($q);

    $stmt = $c->prepare($q);
    if (!$stmt) die('prepare() failed');

    $stmt->execute() or die;
    $stmt->store_result() or die;
    if ($stmt->num_rows <= 0) {
        echo "<h1>No results</h1>";
        exit;
    }

    ?>
    <div id='product-content'>
        <form method='get' action='product.php'>
            <fieldset>
                <hr>
                <div class='field-list'>
                    <label for='q'>
                        <input name='q' size=40 placeholder='Search entire store here..'/>

                     </label>
                    <button name='product-query-submit'>Search</button>
                </div>
            </fieldset>
            <hr>
        </form>
        <span>(<?=$stmt->num_rows?>) results</span>
        <div class='product-row'>
            <div class='box box-all'>
                <span class='header'>Product Name</span>
            </div>
            <div class='box box-all'>
                <span class='header'>Product Type</span>
            </div>
            <div class='box box-all'>
                <span class='header'>Product Brand</span>
            </div>
            <div class='box box-all'>
                <span class='header'>Product Price</span>
            </div>
            <div class='box box-all'></div>
            <span class='stretch'></span>
        </div>
        <?php

        $stmt->bind_result($pid, $pname, $ptype, $pbrand, $price) or die;
        while($stmt->fetch()) {
        ?>
            <div class='product-row'>
                <div class='box box-all'>
                    <a href="product.php?pid=<?=$pid?>">
                        <?=$pname?>
                    </a>
                </div>
                <div class='box box-all'><?=$ptype?></div>
                <div class='box box-all'><?=$pbrand?></div>
                <div class='box box-all'><?=$price?></div>
                <div class='box box-all'>
                    <a href='cart.php?operation=add&id=<?=$pid?>'>
                        <img src='<?=\Common\Functions::linkAsset('img', 'Shopping_Cart.jpg')?>'
                             width=50 height=40 />
                    </a>
                </div>
                <span class='stretch'></span>
            </div>
    <?php
    }
    ?>
    <?php
} else if (isset($_GET['page'])) {

$s = \Core\db::get()->select(
    array('COUNT(product_id)'),
    'product'
);

$s->execute() or die;
$s->bind_result($count) or die;
$s->fetch() or die;
$s->close();

?>
<div id='pagination-container'>
    <ul>
    <?php
    $pages = ceil($count / $ITEMS_PER_PAGE);
    for($i = 1; $i <= $pages; $i++) {
        ?>
        <li>
            <a href='product.php?page=<?=$i?>'>
                <?=$i?>
            </a>
        </li>
        <?php
    }
    ?>
    </ul>
</div>
<?php

$page = \Common\Field::GETValid('page');
if (!is_numeric($page))
    die('Wrong type');

if ($page <= 0) {
    header('Location: ' . "product.php?page=1");
    exit;
}

$BEGIN = ($page - 1) * $ITEMS_PER_PAGE;
$q = "SELECT `product_id`,
        `product_name`, `product_type`, `product_brand`,`unit_price`
        FROM `product` LIMIT ?, ?";
$stmt = \Core\db::get()->squery($q);
if (!$stmt) die('Database error');
$stmt->bind_param('ii', $BEGIN, $ITEMS_PER_PAGE) or die('Database error');

$stmt->execute() or die('Database error');
$stmt->store_result() or die('Database error');

?>
<div id='query-container'>
    <form method='get' action='product.php'>
        <fieldset>
            <hr>
            <div class='field-list'>
                <label for='q'>
                    <input name='q' size=40 placeholder='Search entire store here..'/>
                </label>
                <button name='product-query-submit'>Search</button>
            </div>
        </fieldset>
        <hr>
    </form>
</div>
<?php
if ($stmt->num_rows <= 0)
    echo "<h1>No results</h1>";
else {
    ?>
    <div id='listing-container'>
    <div class='product-row'>
        <div class='box box-all'>
            <span class='header'>Product Name</span>
        </div>
        <div class='box box-all'>
            <span class='header'>Product Type</span>
        </div>
        <div class='box box-all'>
            <span class='header'>Product Brand</span>
        </div>
        <div class='box box-all'>
            <span class='header'>Product Price</span>
        </div>
        <div class='box box-all'></div>
        <span class='stretch'></span>
    </div>
    <?php
    $stmt->bind_result($pid, $pname, $ptype, $pbrand,$price) or die('Database error');
    while($stmt->fetch()) {
        ?>
        <div class='product-row'>
            <div class='box box-all'>
                <a href='product.php?pid=<?=$pid?>'>
                    <?=$pname?>
                </a>
            </div>
            <div class='box box-all'><?=$ptype?></div>
            <div class='box box-all'><?=$pbrand?></div>
            <div class='box box-all'>$<?=$price?></div>
            <div class='box box-all'>
                <a href='cart.php?operation=add&id=<?=$pid?>'>
                    <img src='<?=\Common\Functions::linkAsset('img', 'Shopping_Cart.jpg')?>'
                         width=50 height=40 />
                </a>
            </div>
            <span class='stretch'></span>
        </div>
    <?php
    }
    ?>
    </div>
    <?php

    $stmt->close() or die('Database error');
    }
?>
</div>
<?php
} else if (isset($_GET['pid'])) {
    ?>
    <div id='product-specific-content'>
    <?php
    $pid = \Common\Field::GETValid('pid');
    $modifier = '';

    $stmt = \Core\db::get()->select(
        array('product_id', 'product_name', 'product_type', 'product_brand', 'product_summary', 'unit_price'),
        'product',
        array('product_id'),
        1
    );

    $stmt->bind_param('s', $pid) or die;
    $stmt->execute() or die;
    $stmt->store_result() or die;

    if ($stmt->num_rows <= 0)
        throw new Exception ('unknown product request');

    $stmt->bind_result($pid, $pname, $ptype, $pbrand, $psum, $pprice) or die;
    $stmt->fetch() or die;

    if (\Common\Access::is_user_admin())
        $modifier = 'required';
    else
        $modifier = 'readonly';
    ?>
    <form action='cart.php'>
        <input type='hidden' name='operation' value='add' />
        <input type='hidden' name='id' value="<?=$pid?>" />
        <input type='submit' class='comment-submit' value='Add item to Cart'/>
    </form>

    <fieldset>
    <legend>
        Images
    </legend><hr>
    <?php

    if (\Common\Access::is_user_admin()) {
        ?>
        <form action='edit.php'
              method='post' id='product-images' enctype='multipart/form-data'>
            <input name='product[id]' type='hidden' value="<?=$pid?>" />
            <div class='field-list'>
                <label for='product-images[]'>
                    <input type='file' name='product-images[]' multiple>
                    <input type='hidden' name='MAX_FILE_SIZE' value='4096000'> <!-- 500kb -->
                </label>
            </div>
        <button name='product[image-submit]'>modify images</button>
    <?php
    }
    $stmt = \Core\db::get()->select(
    array('image_id', 'image_ext'),
    'product_images',
    array('fk_product_id')
    );

    $stmt->bind_param('s', $pid) or die;
    $stmt->execute() or die;
    $stmt->store_result() or die;
    if ($stmt->num_rows > 0) {
    $stmt->bind_result($image_id, $image_ext) or die;
    ?>
    <div class='main-image-container'>
    <?php
    while($stmt->fetch()) {
        $img_url = \Common\Functions::linkAsset(
            'product_images', $pid . '/' .
            $image_id . '.' . $image_ext);
        ?>
        <div class='image-container'>
            <?php
            if (\Common\Access::is_user_admin()) {
                ?>
                <label for='image-delete'>
                    Delete image
                    <input type='checkbox' name='image-cb[]'
                           value='<?=$pid?>:<?=$image_id?>' />
                </label>
            <?php
            }
            ?>
            <div class='image-src'>
                <img class='image-preview' src='<?=$img_url?>' />
            </div>
        </div>
            <?php
            }
        if (\Common\Access::is_user_admin()) {
            ?>
            </form>
            <?php
        }
            ?>
    </div>
    </fieldset>
<?php
}
?>
<input type='hidden' name='product[id]' value='<?=$pid?>' />
<fieldset class='product-gen-info'>
    <legend>Product Information</legend><hr>
    <div class='field-list'>
        <label for='product[name]'>
            Name
            <input name='product[name]' value="<?=$pname?>"
                   size=30 maxlength=30 <?=$modifier?>/>
        </label>
    </div>
    <div class='field-list'>
        <label for='product[type]'>
            Type
            <input name='product[type]' value='<?=$ptype?>'
                   size=30 maxlength=30 <?=$modifier?>/>
        </label>
        <label for='product[brand]'>
            Brand
            <input name='product[brand]' value='<?=$pbrand?>'
                   size=30 maxlength=30 <?=$modifier?> />
        </label>
    </div>
    <div class='field-list'>
        <label for='product[price]'>
            Price
            <input name='product[price]' value='<?=$pprice?>'
                   size=30 maxlength=30 <?=$modifier?> />
        </label>
    </div>
    <div class='field-list'>
        <label for='product[summary]'>
            Product Summary
            <textarea name='product[summary]'
                cols=80
                rows=10
                ><?=urldecode($psum)?></textarea>
        </label>
    </div>
    <?php
    if (\Common\Access::is_user_admin()) {
    ?>
    <div class='field-list'>
        <button name='product[info-submit]'>edit product</button>
    </div>
    <?php
    }
    ?>
    </fieldset>
</form>
    <?php
        if (isset($_SESSION['loggedin'])) {

            $s = \Core\db::get()->select(
                array('user_name'),
                'user',
                array('userid'),
                1
            );

            $s->bind_param('i', $_SESSION['uid']) or die;
            $s->execute() or die;

            $user = '[INVALID USER]';
            $s->bind_result($user) or die;
            $s->fetch() or die;
            $s->close() or die;
            ?>
        </form>
            <fieldset>
                <legend>ADD A COMMENT</legend><hr>
                <form action='comment.php' method='post'>

                    <input type='hidden' name='comment[pid]' value='<?=$pid?>' />

                    <div class='comment-panel'>
                        <label for='comment[data]'>
                            <input style='comment-textfield' name='comment[data]' maxlength=200
                                   size=100
                                   placeholder='Add a comment...'/>
                        </label>
                    </div>
                    <div class='comment-bottom-right-panel'>
                        <span>
                            Posting as <?=$user?>
                        </span>
                        <label for='comment[submit]'>
                            <input type='submit' name='comment[submit]' class='comment-submit' value='Comment'/>
                        </label>
                    </div>
                </form>
            </fieldset>
        <?php
        }

        $query = "SELECT `pc`.`comment_id`, `pc`.`fk_userid`, `pc`.`comment`,
        DATE(`pc`.`date_added`), TIME(`pc`.`date_added`), `u`.`image_url`
        FROM `product_comment` `pc`
        JOIN `user` `u` ON `u`.`userid` = `pc`.`fk_userid`
        WHERE `pc`.`fk_product_id` = ?";

        $c = \Core\db::get()->connect();
        $stmt = $c->prepare($query);
        if (!$stmt)
            die('prepare() failed');

        $stmt->bind_param('s', $pid) or die;
        $stmt->execute() or die;
        $stmt->store_result() or die;
        if ($stmt->num_rows > 0) {
            ?>
            <fieldset>
            <legend>(<?=$stmt->num_rows?>) comments</legend><hr>
                <?php
                $stmt->bind_result($cid, $uid, $data, $date, $time, $image_url) or die;
                $ustmt = \Core\db::get()->select(
                    array('user_name'),
                    'user',
                    array('userid'),
                    1
                );
                while($stmt->fetch()) {
                    $ustmt->bind_param('i', $uid) or die;
                    $ustmt->execute() or die;
                    $ustmt->store_result() or die;

                    $ustmt->bind_result($username) or die;
                    $ustmt->fetch() or die;

                    ?>
                    <div class='comment-box' id='<?=$uid.':'.$cid?>'>
                        <div class='user-toolbox'>
                            <?php
                            if (isset($_SESSION['loggedin']) &&
                                ($_SESSION['uid'] == $uid || $_SESSION['level']) >= \Common\Access::getUserLevels()['ADMIN_BASIC']
                            ) {
                                ?>
                                <div class='delete-comment-box'>
                                    <span class='delete-comment'>
                                        <a href='comment.php?a=del&cid=<?=$cid?>'>
                                            <img class='user-x-comment' src='<?=\Common\Functions::linkAsset('img', 'x.jpg')?>' />
                                        </a>
                                    </span>
                                </div>
                            <?php
                            }
                            if (!empty($image_url)) {
                            ?>
                            <div class='image' style="display: inline-block;">
                                <img src='<?=\Common\Functions::linkAsset('user_images/' . $uid, $image_url)?>'
                                     width=44 height=44/>
                            </div>
                            <?php
                            }
                            ?>
                            <span class='username'>
                                <?=$username?>
                            </span>
                            <span class='date'>
                                said on <?=$date?>
                            </span>
                            <span class='time'>
                                at <?=$time?>
                            </span>
                        </div>
                        <div class='comment-data'>
                            <p class='data-indent'>
                                <?=$data?>
                            </p>
                        </div>
                    </div>
                    <?php
                }
            ?>
            </fieldset>
            <?php
        }
} else {
    http_response_code(301);
    header('Location: ' . "product.php?page=1");
    exit;
}
?>
</div>