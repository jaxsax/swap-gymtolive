<?php
$starttime = microtime();
require_once 'core/CommonFunctions.php';
require_once 'core/CommonAccess.php';
require_once 'core/SessionHandler.php';
require_once 'core/ErrorHandler.php';

include_once 'core/html/HTMLInfo.php';
include_once 'core/html/HTMLTemplator.php';

require_once 'core/https_config.php';

set_exception_handler('\Core\ErrorHandler::exceptionHandler');
\Session\SessionHandler::start('_GTL_LOGIN');

$SessionMIN = 10;
$SessionSEC = $SessionMIN * 60;

if (isset($_SESSION['loggedin'])) {
    if (isset($_SESSION['sess_begin'])) {
        $inactive = time() - $_SESSION['sess_begin'];
        if ($inactive >= $SessionSEC) {
            \Session\SessionHandler::destroy();
            header('Location: ' . "login.php");
        }
    }

    $_SESSION['sess_begin'] = time();
}

if (isset($_SESSION['loggedin'])) {

    $uid = $_SESSION['uid'];
    $username = $_SESSION['username'];
    $access_level = \Common\Access::getUserLevels()['INVALID'];

    //$qs = "SELECT `user_name`, `access_level`, `image_url` FROM `user` WHERE `user`.`userid` = ? LIMIT 1";
    $stmt = \Core\db::get()->select(
        array('user_name', 'access_level', 'image_url'),
        'user',
        array('userid'),
        1
    );

    $stmt->bind_param('i', $uid) or die;
    $stmt->execute() or die;
    $stmt->store_result() or die;
    if ($stmt->num_rows <= 0) {
        /* user session expired? */
        \Session\SessionHandler::destroy();
        header('Location: ' . "index.php");
        exit;
    }

    $stmt->bind_result($user, $access, $image_url) or die;
    $stmt->fetch() or die;

    $access_level = $access;

    $admin_url = "admin.php";
    $manage_url = "manage_profile.php?uid=$uid";
    $logout_url = "login.php?s=logout";
    $cart_url = "cart.php";
}

?>

<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='utf-8'>
    <title><?php echo \HTML\Info::getValue('TITLE');?></title>
    <?php \HTML\Templator::getInstance()->link_css(); ?>

    <!--[if lt IE 9]
    <?php echo "<script src='" . \Common\Functions::linkAsset('js', 'html5shiv.js') . "></script>\n"; ?>
    <![endif]-->
</head>
<body>

<nav>
    <div id='top-common'>
        <div id='logo'>
            <img src='<?=\Common\Functions::linkAsset('img', 'banner.jpg')?>' alt='main-logo' />
        </div>
        <div id='user-nav'>
            <?php
            if (isset($_SESSION['loggedin'])) {
            ?>
            <div id='user-info-container'>
                <h2>
                    <?php
                    if (!empty($image_url)) {
                    ?>
                    <img src='/gtl/static/user_images/<?=$uid?>/<?=$image_url?>' width=40 height=40/>
                    <?php
                    }
                    ?>
                    Welcome,<?=$username?></h2>
                <?php
                if (\Common\Access::is_user_admin()) {
                ?>
                    <span>
                        <a href='<?=$admin_url?>'>Administrate website</a>
                    </span>
                <?php
                }
                ?>
                    <span><a href='<?=$cart_url?>'>View my cart</a></span>
                    <span><a href='<?=$manage_url?>'>Manage profile</a></span>
                    <span><a href='<?=$logout_url?>'>Logout</a></span>
                </div>
                <?php
            } else {
                ?>
                <form action='https://localhost/gtl/login.php' method='post'>
                    <div id='user-input-info'>
                        <input type='text' name='_username' placeholder='Username' size=30 maxlength=30 autofocus />
                        <input type='password' name='_pwd' placeholder='Password' size=30 maxlength=30 />
                        <button name='_login_submit'>Login</button>
                    </div>
                </form>
                    <a href='register.php'>Sign up</a>
                <?php
            }
        ?>

        </div>
    </div>
    <ul id='main-nav'>
        <?php
        /* [key] = url
             * [value] = string */
        foreach (\Common\Functions::$URL_TABLE as $key => $value) {
            echo "<li><a href='$key' class='alink'>" . \Common\Functions::getURLValue($key) . "</a></li>\n";
        }
    ?>
    </ul>
</nav>

<div id='content'>