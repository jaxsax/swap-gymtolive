<div id='verify'>
<?php

require_once 'core/CommonFunctions.php';
require_once 'core/CommonSecurity.php';
require_once 'core/CommonAccess.php';
require_once 'core/CommonField.php';
require_once 'core/CommonSecurity.php';
require_once 'core/dbhandler.php';
require_once 'core/SessionHandler.php';

include_once 'core/html/HTMLHelper.php';


$First_Name = \Common\Field::POSTValid('fname');
$Last_Name = \Common\Field::POSTValid('lname');

$User_Name = \Common\Field::POSTValid('uname');
$Password = \Common\Field::POSTValid('password');
$Confirm_Password = \Common\Field::POSTValid('cpassword');

$Email_Address = \Common\Field::POSTValid('emailaddress');
$Gender = \Common\Field::POSTValid('gender');
$Birthday = \Common\Field::POSTValid('birth_date');

?>
<form action='register.php' method='post'>

<input type='hidden' name='fname' value='<?=$First_Name?>' />
<input type='hidden' name='lname' value='<?=$Last_Name?>' />
<input type='hidden' name='uname' value='<?=$User_Name?>' />
<input type='hidden' name='emailaddress' value='<?=$Email_Address?>' />
<input type='hidden' name='gender' value='<?=$Gender?>' />
<input type='hidden' name='date' value='<?=$Birthday?>' />
<?php

/* checking if fields are empty */
if (!($no_error = \HTML\TFL_valid(array($First_Name, $Last_Name, $User_Name, $Email_Address, $Password))))
{
    echo "<h1>Please fill up all fields!</h1>\n";
}
/* check if confirm password is filled */
else if (!($no_error = \HTML\TF_valid($Confirm_Password)))
{
    echo "<h1>Please enter confirm password</h1>\n";
}
/* check if passwords meet criteria of being alphanumeric with minimum 8 maximum 30 characters */
else if (!($no_error = preg_match('/^.*(?=.{8,})(?=.*[0-9])(?=.*[a-zA-Z]).*$/', $Password)))
{
    echo "<h1>
            <p style='font-size:20px'>
            <b>Password should contain:</b>
                    <br>
                    <br>
        <ul>
            <li>
                - At least 8 minimum characters.
            </li>
        </ul>
                    <br>
        <ul>
            <li> - Big capital letters.</li>
        </ul>
                    <br>
         <ul>
            <li> - Small capital letters.</li>
        </ul>
                    <br>
        <ul>
            <li> - Numerical values.</li>
        </ul>
          </h1>";
}
/* passwords equal? */
else if (!($no_error = ($Password == $Confirm_Password))) {
    echo "<h1>Passwords not equal\n";
}

$user_exists = false;

/* checking if the user already exists in database */
if ($no_error) {

    $stmt = \Core\db::get()->select(
        array("COUNT('username')"),
        'user',
        array('user_name')
    );

    $stmt->bind_param('s', $User_Name) or die;
    $stmt->execute() or die;
    $stmt->bind_result($count) or die;
    $stmt->fetch() or die;

    /* user already exists */
    if ($count >= 1) {
        $no_error = false;
        $user_exists = true;
    }

    $stmt->close() or die;
}

if ($user_exists) {
    ?>
    <h1>User already exists!</h1>
    <button>Back</button>
    <?php
    exit;
}

/* inserting into generic table `user` */
if ($no_error) {

    $c = \Core\db::get()->connect();

    $qs = "INSERT INTO `user`(
    `first_name`, `last_name`, `user_name`, `email_address`, `password`, `salt`)
    VALUES(?,?,?,?,?,?)";

    $stmt = $c->prepare($qs);
    if (!$stmt)
        die('user prepare() failed');

    require_once 'core/Validation.php';

    $dbsalt = \Common\Security::generateRandChar();
    $Password = \Common\Validation::cleanXSS($Password);
    $dbpw = \Common\Security::generatePassword($Password, $dbsalt);

    $First_Name = \Common\Validation::cleanXSS($First_Name);
    $Last_Name = \Common\Validation::cleanXSS($Last_Name);
    $User_Name = \Common\Validation::cleanXSS($User_Name);
    $Email_Address = \Common\Validation::cleanXSS($Email_Address);

    $stmt->bind_param('ssssss', $First_Name, $Last_Name, $User_Name, $Email_Address, $dbpw, $dbsalt) or die;
    $stmt->execute() or die;

    if ($stmt->affected_rows <= 0) {
        throw new Exception('add user failed');
    }

    $qs = "INSERT INTO `gtl`.`customer`
    (`fk_userid`, `gender`, `membership_status`)
    VALUES (?, ?, ?)";

    $default_member = \Common\Access::getUserLevels()['CUSTOMER'];
    $last_insert_id = $c->insert_id;

    $stmt = $c->prepare($qs);
    if (!$stmt)
        die('customer prepare() failed');

    /* because only common data is during registration, empty values are inserted in */
    $stmt->bind_param('isi', $last_insert_id, $Gender, $default_member) or die;
    $stmt->execute() or die;

    if ($stmt->affected_rows <= 0) {
        throw new Exception('add customer failed');
    }

    \Session\SessionHandler::start('_GTL_LOGIN');
    if (\Session\SessionHandler::checkLogin($User_Name, $Password)) {

        $stmt->close();
        \Core\db::get()->close_con();

        header('Location: ' . "index.php");
        exit;
    }
} else
    die("<button onclick='window.history.back();'>Please fill in the requirement!</button>");


?>
</form>
</div>