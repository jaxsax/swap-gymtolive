<?php

require_once 'core/CommonFunctions.php';
require_once 'core/CommonField.php';
require_once 'core/CommonSecurity.php';
require_once 'core/Validation.php';

require_once 'core/dbhandler.php';
require_once 'core/SessionHandler.php';
require_once 'core/html/HTMLHelper.php';

$ADMIN_MASK = 1 << 0;
$MODIFY_SELF_MASK = 1 << 1;

$MODIFICATION_MASK = 0;

define('USER_IMAGE_URL', 'user_images');

if (isset($_SESSION['aauth'])) {
    $MODIFICATION_MASK += $ADMIN_MASK;
} else if (!isset($_SESSION['bauth'])) {
    header('Location: ' . "redirect.php?r=manage_profile.php?uid=" . \Common\Field::GETValid('uid'));
    exit;
}

if (!empty($_POST['uid'])) {

    $uid = $_POST['uid'];

    if (\Common\Access::getUserState() == \Common\Access::getAccountStates()['UNACTIVATED']) {
        header('Location: ' . "activate.php?a=react");
        exit;
    }

    $token_valid = !empty($_POST['token']) && \Common\Validation::CSRFToken_validate($_POST['token']);
    if (!$token_valid) {
        throw new Exception('Invalid CSRF token');
    }

    if (isset($_POST['_personal-info_submit'])) {

        if ($_SESSION['uid'] != $uid && !\Common\Access::is_user_admin()) {
            die('unauthorized profile modification');
        }

        $fname = \Common\Field::POSTValid('fname'); /* MANDATORY */
        $lname = \Common\Field::POSTValid('lname'); /* MANDATORY */
        $umail = \Common\Field::POSTValid('uemail'); /* MANDATORY */

        $ucontact = \Common\Field::POSTValid('ucontact');
        $uaddr = \Common\Field::POSTValid('uaddr');
        $upcode = \Common\Field::POSTValid('upcode');

        $mandatory_fields = array($fname, $lname, $umail);
        if (!\Common\Validation::validateFieldList('\Common\Validation::isValidText', $mandatory_fields)) {
            die('Not all fields are filled!');
        }

        if(!preg_match('/^\d{8}$/', $ucontact))
        {
            header('Location: http://localhost/gtl/fail_page.php?uid=' . $uid . '&error=2');
            exit;
        }

        /* match for all digits */
        if(is_numeric($uaddr))
        {
            header('Location: http://localhost/gtl/fail_page.php?uid=' . $uid . '&error=3');
            exit;
        }

        if(!preg_match('/^\d{6}$/', $upcode))
        {
            header('Location: http://localhost/gtl/fail_page.php?uid=' . $uid . '&error=4');
            exit;
        }

        if (!empty($_FILES['uimgurl']['name'][0])) {
            $ordered_array = diverse_array($_FILES['uimgurl']);
            $ordered_array = validate_array($ordered_array);

            $generic_path = \Common\Functions::$INFO_TABLE['FOLDER_URL'] .
                '/static' .
                '/' . USER_IMAGE_URL;
            $image_path_dir = $_SERVER['DOCUMENT_ROOT'] . '/' . $generic_path;
            $uimageurl = $ordered_array[0]['name'] . '.' . $ordered_array[0]['ext'];

            foreach($ordered_array as $value) {

                if ($value['size'] == -1) {
                    die('Invalid image size, max 350kB');
                }

                if ($value['name'] == null) {
                    die('invalid file extension, was not recoverable');
                }

                if ($value['tmp_name'] == null) {
                    die('invalid image, only jpeg and png is allowed');
                }

                $img_disk_path_dir = $image_path_dir . '/' . $uid;
                if (!is_dir($img_disk_path_dir))
                    mkdir($img_disk_path_dir, 0, true);

                $img_disk_path = $img_disk_path_dir . '/' . $value['name'] . '.' . $value['ext'];

                move_uploaded_file($value['tmp_name'], $img_disk_path);
            }

            /* Check if the user has an existing picture */
            $image_query = \Core\db::get()->select(
                array('image_url'),
                'user',
                array('userid'),
                1
            );

            $image_query->bind_param('i', $uid) or die;
            $image_query->execute() or die;
            $image_query->store_result() or die;
            if ($image_query->num_rows > 0) {
                /* delete image */
                $image_query->bind_result($image_id) or die;
                $image_query->fetch() or die;
                $image_query->close();

                if (!empty($image_id)) {
                    $del_img_path = $img_disk_path_dir . '/' . $image_id;
                    if (!unlink($del_img_path)) {
                        die('Delete image failed');
                    }
                }
            }

            $c = \Core\db::get()->connect();
            $image_query = "UPDATE `user` SET `image_url` = ? WHERE `userid` = ?";
            $image_stmt = $c->prepare($image_query);
            if (!$image_stmt) {
                die('image prepare() failed');
            }

            $image_stmt->bind_param('ss', $uimageurl, $uid) or die;
            $image_stmt->execute() or die;
            if ($image_stmt->affected_rows <= 0)
                die('Unable to update image');
        }

        $user_qs = "UPDATE `user` SET
        `user`.`first_name` = ?,
        `user`.`last_name` = ?,
        `user`.`email_address` = ?
        WHERE `user`.`userid` = ?";

        $cust_qs = "UPDATE `customer` SET contact_number = ?, address = ?, postal_code = ? WHERE fk_userid = ?";

        $c = \Core\db::get()->connect();
        $stmt = $c->prepare($user_qs);

        if (!$stmt) die('Database error');

        $stmt->bind_param('sssi', $fname, $lname, $umail, $uid) or die ('Database error');
        $stmt->execute() or die ('Database error');
        $stmt->close() or die ('Database error');

        $stmt = $c->prepare($cust_qs);
        if ($stmt) {
            $stmt->bind_param('sssi', $ucontact, $uaddr, $upcode, $uid) or die ('Database error');
            $stmt->execute() or die ('Database error');
            $stmt->close() or die('Database error');
        }

        header('Location: ' . "update_success.php?uid=$uid");
        exit;

    } else if (isset($_POST['_login-info_submit'])) {

        $password = \Common\Field::POSTValid('ucpassword');
        $newpassword = \Common\Field::POSTValid('unpassword');
        $cpassword = \Common\Field::POSTValid('uncpassword');

        if (!\HTML\TFL_valid(array($password, $newpassword, $cpassword))) {
            die('Please fill in all password fields');
        }

        $qs = "SELECT `u`.`salt`, `u`.`password` FROM `user` `u` WHERE `u`.`userid` = ? LIMIT 1";
        $stmt = \Core\db::get()->squery($qs);
        if (!$stmt) die('Database error');

        $stmt->bind_param('i', $uid) or die('Database error');
        $stmt->execute() or die('Database error');
        $stmt->store_result() or die('Database error');

        if ($stmt->num_rows <= 0)
            die('User does not exist');

        $stmt->bind_result($dbsalt, $dbpassword) or die('Database error');
        $stmt->fetch() or die('Database error');

        $hashed_input_password = \Common\Security::generatePassword($password, $dbsalt);

        if (!preg_match('/^.*(?=.{8,})(?=.*[0-9])(?=.*[a-zA-Z]).*$/', $newpassword)) {
            header('Location: http://localhost/gtl/fail_page.php?uid=' . $uid . '&error=1');
            exit;
        }

        if (!\Common\Security::slowEq($hashed_input_password, $dbpassword)) {
            die('Your current password does not match your old password');
        }

        if ($newpassword != $cpassword)
            die('Please ensure new password and confirm password matches each other');

        $new_salt = \Common\Security::generateRandChar();
        $new_password = \Common\Security::generatePassword($newpassword, $new_salt);

        $qs = "UPDATE `user` SET `password` = ?, `salt` = ? WHERE `userid` = ?";
        $c = \Core\db::get()->connect();
        $stmt = $c->prepare($qs);

        if (!$stmt) die('Database error');

        $stmt->bind_param('ssi', $new_password, $new_salt, $uid) or die('Database error');
        $stmt->execute() or die('Database error');

        if ($stmt->affected_rows <= 0) {
            die('Update password failed');
        }

        header('Location: ' . "login.php?s=logout");
        exit;

    } else if (isset($_POST['admin']['submit'])) {

        /* Cant use empty here, CUSTOMER(0) evaluates to "empty" as defined in method description */
        if (!isset($_POST['admin']['level'])) {
            die('require access level to be set');
        }
        if (!isset($_POST['user']['state'])) {
            die('require account state to be set');
        }

        $level = $_POST['admin']['level'];
        $state = $_POST['user']['state'];

        if (!is_numeric($level) || !is_numeric($state)) {
            die('Invalid access type');
        }
        /* Check if the user can set access level */
        if ($_SESSION['level'] < $level)
            die('Access denied, you can not set level tiers equal to your own or higher');

        $query = "UPDATE `user` SET `access_level` = ?, `account_state` = ? WHERE `userid` = ?";
        $c = \Core\db::get()->connect();

        $s = $c->prepare($query);
        if (!$s) {
            die('prepare() failed');
        }

        $s->bind_param('iii', $level, $state, $uid) or die;
        $s->execute() or die;
        if ($s->affected_rows <= 0) {
            die('unable to update access levels');
        }
        $s->close();

        /* Check if user already exists in admin table */
        $s = \Core\db::get()->select(
            array('fk_userid'),
            'admin',
            array('fk_userid'),
            1
        );

        $s->bind_param('i', $uid) or die;
        $s->execute() or die;
        $s->store_result() or die;
        if ($s->num_rows <= 0 && $level >= \Common\Access::getUserLevels()['ADMIN_BASIC']) {

            $s->close();

            $query = "INSERT INTO `admin` VALUES(?)";
            $s = $c->prepare($query);
            if (!$s)
                die('prepare() failed');

            $s->bind_param('i', $uid) or die;
            $s->execute() or die;
            if ($s->affected_rows <= 0)
                die('unable to add user to admin');
        } else if ($s->num_rows > 0 && $level <= \Common\Access::getUserLevels()['ADMIN_BASIC']) {

            $s->close();

            $query = "DELETE FROM `admin` WHERE `fk_userid` = ?";
            $s = $c->prepare($query);
            if (!$s)
                die('prepare() failed');

            $s->bind_param('i', $uid) or die;
            $s->execute() or die;
            if ($s->affected_rows <= 0)
                die('unable to remove user from admin');
        }

        header('Location: ' . "update_success.php?uid=$uid");
        exit;
    }
} else if (isset($_GET['uid'])) {
    /* Viewing someones profile */
    $uid = \Common\Field::GETValid('uid');

    $field_modifier = '';
    if ($_SESSION['uid'] == $uid) {
        $field_modifier .= 'required';
        $MODIFICATION_MASK += $MODIFY_SELF_MASK;
    } else if (\Common\Access::is_user_admin()) {
        $field_modifier .= 'required';
    } else {
        $field_modifier .= 'readonly';
    }

    $uq = "SELECT
    `user_name`, `first_name`, `last_name`, `email_address`, `image_url`,
     `address`, `postal_code`, `contact_number`
    FROM `user`
    JOIN `customer` ON `fk_userid` = `userid`
    WHERE `userid` = ? LIMIT 1";

    $stmt = \Core\db::get()->squery($uq);
    if (!$stmt) die('Database error');

    $stmt->bind_param('i', $uid) or die('Database error');
    $stmt->execute() or die('Database error');
    $stmt->store_result() or die('Database error');

    if ($stmt->num_rows <= 0)
        throw new Exception('unknown userid ' . $uid);

    $stmt->bind_result($uname, $fname, $lname, $email, $uimgurl, $address, $pcode, $contact) or die('Database error');
    $stmt->fetch() or die('Database error');

    ?>
    <div id='manage-profile'>
    <?php
    if ($MODIFICATION_MASK & ($ADMIN_MASK | $MODIFY_SELF_MASK)) {
    ?>
        <form action='manage_profile.php' method='post' enctype="multipart/form-data">
        <input name='uid' value='<?=$uid?>' type='hidden'>
        <input type='hidden' name='token' value='<?=\Common\Validation::CSRFToken_generate()?>' />
    <?php
    }
    ?>
        <fieldset id='personal-info'>
            <legend>
                <?php

                if ($MODIFICATION_MASK & $MODIFY_SELF_MASK) {
                    echo "personal particulars";
                } else if (!($MODIFICATION_MASK & $MODIFY_SELF_MASK) && $MODIFICATION_MASK & $ADMIN_MASK) {
                    echo "Administrating profile " . $uname;
                } else
                    echo "Viewing profile " . $uname;
                ?>
            </legend><hr>
            <div class='field-list'>
                <label for='fname'>
                    First Name
                    <input name='fname' value=<?=$fname?> size=30 maxlength=30 <?=$field_modifier?> />
                </label>
                <label for='lname'>
                    Last Name
                    <input name='lname' value=<?=$lname?> size=30 maxlength=30 <?=$field_modifier?> />
                </label>
            </div>
            <div class='field-list'>
                <label for='uemail'>
                    Email Address
                    <input name='uemail' value=<?=$email?> size=30 maxlength=30 type='email' <?=$field_modifier?> />
                </label>
            </div>
            <div class='field-list'>
                <label for='uaddr'>
                    Address
                    <input name='uaddr' value='<?=$address?>' size=50 maxlength=50 type='text' <?=$field_modifier?> />
                </label>
            </div>
            <div class='field-list'>
                <label for='ucontact'>
                    Contact Number
                    <input name='ucontact' value='<?=$contact?>' size=12 maxlength=8 type='text' <?=$field_modifier?> />
                </label>
                <label for='upcode'>
                    Postal Code
                    <input name='upcode' value='<?=$pcode?>' size=12 maxlength=6 type='text' <?=$field_modifier?> />
                </label>
            </div>
            <?php
            if ($MODIFICATION_MASK & $MODIFY_SELF_MASK) {
            ?>
            <div class='field-list'>
                <label for='uimgurl[]'>
                    Image URL
                    <?php
            }
            if (!empty($uimgurl)) {
                ?>
                <div id='user-image-container'>
                    <img src='/gtl/static/<?=USER_IMAGE_URL?>/<?=$uid?>/<?=$uimgurl?>' width=144 height=144/>
                </div>
                <?php
            }
            if ($MODIFICATION_MASK & $MODIFY_SELF_MASK) {
            ?>
                    <input name='uimgurl[]' type='file' value='<?=$uimgurl?>'/>
                    <input type='hidden' name='MAX_FILE_SIZE' value='4096000'> <!-- 500kb -->
                </label>
            </div>
            <?php
            }
            ?>
            <?php
            if ($MODIFICATION_MASK & ($ADMIN_MASK | $MODIFY_SELF_MASK)) {
            ?>
                <div class='field-list'>
                    <button name='_personal-info_submit' class='save-changes'>Save Changes</button>
                </div>
            <?php
            }
            ?>

        </fieldset>
        <?php
        if ($MODIFICATION_MASK & ($ADMIN_MASK | $MODIFY_SELF_MASK)) {
        ?>
            <fieldset id='login-info'>
                <legend>login information</legend><hr>
                <div class='field-list'>
                    <label for='ucpassword'>
                        Current Password
                        <input type='password' name='ucpassword' size=30 maxlength=30 />
                    </label>
                </div>
                <div class='field-list'>
                    <label for='unpassword'>
                        New Password
                        <input type='password' name='unpassword' size=30 maxlength=30 />
                    </label>
                </div>
                <div class='field-list'>
                    <label for='uncpassword'>
                        Confirm new password
                        <input type='password' name='uncpassword' size=30 maxlength=30 />
                    </label>
                </div>
                <div class='field-list'>
                    <button name='_login-info_submit' class='save-changes'>Save Changes</button>
                </div>
            </fieldset>
            <fieldset class='deactivate-account'>
                <legend>
                    <a href='login.php?operation=delete' style='color: red;'>
                        deactivate account
                    </a>
                </legend>
            </fieldset>
        <?php
        }
        if ($MODIFICATION_MASK & $ADMIN_MASK) {
            ?>
            <fieldset id='admin-info'>
                <legend>administration panel</legend><hr>
                <form action='manage_profile.php' action='post'>
                    <div class='field-list'>
                        <label for='admin[level]'>
                            Access level
                            <select name='admin[level]'>
                                <?php
                                foreach(\Common\Access::getUserLevels() as $key => $value) {
                                    /* Display promotion values up to a tier below the administrators current rank
                                        EXCEPTION: user has full admin powers
                                    */
                                    if ($_SESSION['level'] == \Common\Access::getUserLevels()['ADMIN_FULL'] ||
                                        $value < $_SESSION['level']
                                        ) {

                                        /* Automatically select values based off the
                                            current queried user */
                                        ?>
                                        <option value='<?=$value?>'
                                            <?php echo \Common\Access::getUserAccess($uid) == $value ? 'selected' : ''?>>
                                            <?=$key?>
                                        </option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </label>
                    </div>
                    <div class='field-list'>
                        <label for='user[state]'>
                            Account state
                            <select name='user[state]'>
                                <?php

                                $s = \Core\db::get()->select(
                                    array('account_state'),
                                    'user',
                                    array('userid'),
                                    1
                                );

                                $s->bind_param('i', $uid) or die;
                                $s->execute() or die;
                                $s->store_result() or die;
                                if ($s->num_rows <= 0)
                                    die('invalid user id');
                                $s->bind_result($state) or die;
                                $s->fetch() or die;
                                $s->close();

                                foreach(\Common\Access::getAccountStates() as $key => $value) {
                                    ?>
                                    <option value='<?=$value?>'
                                        <?php echo $state == $value ? 'selected' : ''?>>
                                        <?=$key?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </label>
                    </div>
                    <div class='field-list'>
                        <button name='admin[submit]'>Save Changes</button>
                    </div>
                </form>
            </fieldset>
            <?php
        }
        ?>
    <?php
}
?>
</div>
<?php

function diverse_array($vector) {
    $result = array();
    foreach($vector as $key1 => $value1)
        foreach($value1 as $key2 => $value2)
            $result[$key2][$key1] = $value2;
    return $result;
}

/* check file name to see if a valid extension is found
    This removes any invalid extensions if it preserves a valid extension
    IE: a.php.jpg => a.jpg
        a.php => null (invalid)

    RETURN: null
    RETURN: [random 5char].[valid extension]
*/
function validate_extension($ext, $ext_arr) {

    foreach($ext_arr as $valid_ext) {
        if ($valid_ext == $ext)
            return $valid_ext;
    }
    return null;
}

/* check if file is uploaded via POST */
function upload_valid($tmp_name) {

    if (!empty($tmp_name) && is_uploaded_file($tmp_name))
        return $tmp_name;

    return null;
}

/* check tmp_name to see if
    1) it is uploaded via HTTP POST
    2) has a valid mimetype via getimagesize

    RETURN: null if (is not uploaded via POST or getimagesize returns false)
*/
function validate_image($img_path, $mtype_arr) {

    if (($tmp_name = upload_valid($img_path)) == null)
        return null;

    $image = getimagesize($tmp_name);
    if (!$image)
        return null;

    foreach($mtype_arr as $mimetype) {
        if ($image['mime'] == $mimetype)
            return $img_path;
    }

    return null;
}

function validate_size($size) {
    $MAX_SIZE = 350 * 1024 * 8; /* 350KB */
    $size_in_kB = ($size / 1024.0) / 8.0;

    if ($size_in_kB > $MAX_SIZE)
        return -1;
    return $size;
}

function validate_array($arr) {

    $valid_ext = array('jpg', 'jpeg', 'png');
    $valid_mtype = array('image/jpg', 'image/jpeg', 'image/png');

    $new_arr = array();

    /* FYI: mimetype sent from client is ignored */
    foreach($arr as $value) {
        /* check extensions, check if ending extension is recoverable */

        if (empty($value['tmp_name']))
            continue;

        $intern_array = array();
        $intern_array['size'] = validate_size($value['size']); /* -1 if error */

        $image = explode('.', $value['name']);
        $intern_array['name'] = \Common\Security::generateRandChar(5);
        $intern_array['ext'] = $image[1];

        // $intern_array['name'] = validate_extension($value['name'], $valid_ext); /* null if error */
        $intern_array['tmp_name'] = validate_image($value['tmp_name'], $valid_mtype); /* null if error */

        $new_arr[] = $intern_array;
    }

    return $new_arr;
}
?>