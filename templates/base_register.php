<div id='registration'>
    <h1>Create an Account</h1>
    <form action='verify.php' method='post'>
        <?php
        $fname = getvalue('fname');
        $lname = getvalue('lname');
        $uname = getvalue('uname');
        $emailaddress = getvalue('emailaddress');
        $gender = getvalue('gender');
        $birthday = getvalue('date');
        ?>
        <fieldset class='personal-info'>
            <legend>personal information</legend><hr>
            <div class='field-list'>
                <label for='fname'>
                    First Name
                    <input name='fname' value='<?=$fname?>' size=30 maxlength=30 required type='text'/>
                </label>
                <label for='lname'>
                    Last Name
                    <input name='lname' value='<?=$lname?>' size=30 maxlength=30 required type='text' />
                </label>
            </div>
            <div class='field-list'>
                <label for='emailaddress'>
                    Email Address
                    <input name='emailaddress' value='<?=$emailaddress?>' size=30 required maxlength=30 type='email' />
                </label>
                <label for='gender'>
                    <select name='gender' value='<?=$gender?>'>
                        <option value='m'>Male</option>
                        <option value='f'>Female</option>
                    </select>
                </label>
            </div>
            <div class='field-list'>
                <label for='birth_date'>
                    Birthdate
                    <input name='birth_date' value='<?=$birthday?>' required type='date' />
                </label>
            </div>
        </fieldset>
        <fieldset class='login-info'>
            <legend>login information</legend><hr>
            <div class='field-list'>
                <label for='uname'>
                    Username
                    <input name='uname' value='<?=$uname?>' size=30 maxlength=30 required type='text' />
                </label>
            </div>
            <div class='field-list'>
                <label for='password'>
                    Password
                    <input name='password' size=30 maxlength=30 type='password' required placeholder='minimum 8 characters' />
                </label>
                <label for='cpassword'>
                    Confirm Password
                    <input name='cpassword' size=30 maxlength=30 required type='password' />
                </label>
            </div>
        </fieldset>

        <fieldset class='submit-button'>
            <button name='register'>Register!</button>
        </fieldset>

        <?php
        function getvalue($key) {
            if (isset($_POST[$key]))
                return $_POST[$key];
            return '';
        }
        ?>
    </form>
</div>
