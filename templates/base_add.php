<?php
require_once 'core/CommonField.php';
require_once 'core/Validation.php';

use Core\db;

\Session\SessionHandler::start('_GTL_LOGIN');
if (!isset($_SESSION['aauth'])) {
    header('Location: /gtl/secure.php?r=admin.php');
    exit;
}

define('PRODUCT_IMAGES_FOLDER', 'product_images');

/* recursive compare & swap product id */
function id_valid($pid) {

    if ($pid == null)
        return id_valid(\Common\Security::generateRandChar(7));

    $exists_query = "SELECT COUNT(*) FROM `product` WHERE `product_id` = ? LIMIT 1";
    $stmt = \Core\db::get()->squery($exists_query);
    if ($stmt) {
        $stmt->bind_param('s', $pid)    or die ('Database error');
        $stmt->execute()                or die ('Database error');
        $stmt->bind_result($count)      or die ('Database error');
        $stmt->fetch()                  or die ('Database error');
        $stmt->close()                  or die ('Database error');

        if ($count >= 1)
            return id_valid(\Common\Security::generateRandChar(7));
        return $pid;
    }
    die('Database error');
}

/*
 * prefer field over list
 * */
function select_field_or_list($field, $list) {

    if (empty($field)) {
        if (empty($list) || $list == 'none')
            return null;
        return $list;
    }

    return $field;
}

function diverse_array($vector) {
    $result = array();
    foreach($vector as $key1 => $value1)
        foreach($value1 as $key2 => $value2)
            $result[$key2][$key1] = $value2;
    return $result;
}

/* check file name to see if a valid extension is found
    This removes any invalid extensions if it preserves a valid extension
    IE: a.php.jpg => a.jpg
        a.php => null (invalid)

    RETURN: null
    RETURN: [random 5char].[valid extension]
*/
function validate_extension($ext, $ext_arr) {

    foreach($ext_arr as $valid_ext) {
        if ($valid_ext == $ext)
            return $valid_ext;
    }
    return null;
}

/* check if file is uploaded via POST */
function upload_valid($tmp_name) {

    if (!empty($tmp_name) && is_uploaded_file($tmp_name))
        return $tmp_name;

    return null;
}

/* check tmp_name to see if
    1) it is uploaded via HTTP POST
    2) has a valid mimetype via getimagesize

    RETURN: null if (is not uploaded via POST or getimagesize returns false)
*/
function validate_image($img_path, $mtype_arr) {

    if (($tmp_name = upload_valid($img_path)) == null)
        return null;

    $image = getimagesize($tmp_name);
    if (!$image)
        return null;

    foreach($mtype_arr as $mimetype) {
        if ($image['mime'] == $mimetype)
            return $img_path;
    }

    return null;
}

function validate_size($size) {
    $MAX_SIZE = 350 * 1024 * 8; /* 350KB */
    $size_in_kB = ($size / 1024.0) / 8.0;

    if ($size_in_kB > $MAX_SIZE)
        return -1;
    return $size;
}

function validate_array($arr) {

    $valid_ext = array('jpg', 'jpeg', 'png');
    $valid_mtype = array('image/jpg', 'image/jpeg', 'image/png');

    $new_arr = array();

    /* FYI: mimetype sent from client is ignored */
    foreach($arr as $value) {
        /* check extensions, check if ending extension is recoverable */

        if (empty($value['tmp_name']))
            continue;

        $intern_array = array();
        $intern_array['size'] = validate_size($value['size']); /* -1 if error */

        $image = explode('.', $value['name']);
        $intern_array['name'] = \Common\Security::generateRandChar(5);
        $intern_array['ext'] = $image[1];

        // $intern_array['name'] = validate_extension($value['name'], $valid_ext); /* null if error */
        $intern_array['tmp_name'] = validate_image($value['tmp_name'], $valid_mtype); /* null if error */

        $new_arr[] = $intern_array;
    }

    return $new_arr;
}

if (isset($_POST['product-add-submit'])) {

    $pname = \Common\Field::POSTValid('product-name'); /* MANDATORY */
    $pprice = \Common\Field::POSTValid('product-price'); /* MANDATORY */
    $psum = \Common\Field::POSTValid('product-summary'); /* OPTIONAL */

    /* MANDATORY: either one*/
    $ptype_field = \Common\Field::POSTValid('product-type-field');
    $ptype_list = \Common\Field::POSTValid('product-type-list');

    /* MANDATORY: either one*/
    $pbrand_field = \Common\Field::POSTValid('product-brand-field');
    $pbrand_list = \Common\Field::POSTValid('product-brand-list');

    $ptype = select_field_or_list($ptype_field, $ptype_list);
    $pbrand = select_field_or_list($pbrand_field, $pbrand_list);

    $mandatory_fields = array($pname, $ptype, $pbrand);
    $ea = \Common\Field::valid_field_length(30, $mandatory_fields);

    if (!empty($ea)) {
        foreach($ea as $error_index) {
            echo "error field: $error_index, field length exceeded<br />";
        }
        die;
    }

    $pid = id_valid(null);
    //$pname = \Common\Validation::cleanXSS($pname);
    $pname = str_replace('"', "'", $pname);
    $psum = urlencode(\Common\Validation::cleanXSS($psum));

    $ptype = \Common\Validation::cleanXSS($ptype);
    $pbrand = \Common\Validation::cleanXSS($pbrand);

    if (!preg_match('/^[0-9]+(\.[0-9]{2})?$/', $pprice)) {
        die('price not properly formatted got[' . $pprice . '] expected DDD:CC');
    }

    $ordered_array = diverse_array($_FILES['product-images']);
    $ordered_array = validate_array($ordered_array);

    $product_insert_query = "INSERT INTO `product`(
    `product_id`, `product_name`, `product_type`, `product_brand`, `product_summary`, `unit_price`)
    VALUES(?,?,?,?,?,?)";

    $con = db::get()->connect();
    $stmt = $con->prepare($product_insert_query);
    if (!$stmt) die('Database error');

    $stmt->bind_param('sssssd', $pid, $pname, $ptype, $pbrand, $psum, $pprice) or die('Database error');
    $stmt->execute() or die($con->error);

    if ($stmt->affected_rows <= 0) {
        die('product was not inserted: ' . $con->error);
    }

    $stmt->close() or die('Database error');

    /* insert images */
    $product_image_query = "INSERT INTO `product_images`(
    `fk_product_id`, `image_id`, `image_ext`)
    VALUES(?,?,?)";

    $stmt = $con->prepare($product_image_query);
    if (!$stmt) {
        die('Database error');
    }

    $generic_path = \Common\Functions::$INFO_TABLE['FOLDER_URL'] .
        '/static' .
        '/' . PRODUCT_IMAGES_FOLDER;

    $image_path_dir = $_SERVER['DOCUMENT_ROOT'] . '/' . $generic_path;

    foreach($ordered_array as $value) {

        if ($value['size'] == -1) {
            die('Invalid image size, max 350kB');
        }

        if ($value['name'] == null) {
            die('invalid file extension, was not recoverable');
        }

        if ($value['tmp_name'] == null) {
            die('invalid image, only jpeg and png is allowed');
        }

        //echo $value['name'] . '<br />';

        $img_disk_path_dir = $image_path_dir . '/' . $pid;
        if (!is_dir($img_disk_path_dir))
            mkdir($img_disk_path_dir, 0, true);

        $img_disk_path = $img_disk_path_dir . '/' . $value['name'] . '.' . $value['ext'];

        move_uploaded_file($value['tmp_name'], $img_disk_path);
        $stmt->bind_param('sss',
            $pid, $value['name'], $value['ext']) or die('Database error');

        $stmt->execute() or die('Database error' . $con->error);
        if ($stmt->affected_rows <= 0) {
            die('Image ' . $pid . ':' . $value['name'] . ' was not inserted ' . $con->error);
        }
    }

    $stmt->close() or die('Database error');
    header('Location: ' . "product.php?pid=$pid");
    exit;
} else {
    $action = \Common\Field::GETValid('a');
    switch($action) {
        case 'product':
        ?>
        <form action='add.php' method='post' enctype='multipart/form-data'>
            <fieldset class='general-info'>
                <legend>General Information</legend><hr>
                 <div class='field-list'>
                    <label for='product-name'>
                        Product Name
                        <input name='product-name' required size='30' maxlength='30'>
                    </label>
                    <label for='product-price'>
                        Price
                        <input name='product-price' required size='7'>
                    </label>
                </div>
            </fieldset>
            <fieldset class='product-branding'>
                <legend>Product Branding</legend><hr>
                <div class='field-list'>
                    <label for='product-type-field'>
                        <input name='product-type-field'
                               placeholder='enter new product type'
                               size='30' maxlength='30'>
                    </label>
                    <label for='product-type-list'>
                        <select name='product-type-list'>
                            <option value='none'>select a product type</option>
                        <?php
                            $product_insert_query = "SELECT `product_type` FROM `product` GROUP BY `product_type`";
                            $stmt = \Core\db::get()->squery($product_insert_query);
                            if ($stmt) {

                                $stmt->execute()            or die ('Database error');
                                $stmt->store_result()       or die ('Database error');
                                if ($stmt->num_rows > 0) {
                                    $stmt->bind_result($type)   or die ('Database error');
                                    while($stmt->fetch()) {
                                        echo "<option value='" . urldecode($type) . "'>" .
                                            urldecode($type) . "</option>";
                                    }
                                }
                                $stmt->close()              or die ('Database error');
                            }
                        ?>
                        </select>
                    </label>
                </div>
                <div class='field-list'>
                    <label for='product-brand-field'>
                        <input name='product-brand-field'
                               placeholder='enter new product brand'
                               size='30' maxlength='30'>
                    </label>
                    <label for='product-brand-list'>
                        <select name='product-brand-list'>
                            <option value='none'>select a product brand</option>
                            <?php
                            $product_insert_query = "SELECT `product_brand` FROM `product` GROUP BY `product_brand`";
                            $stmt = \Core\db::get()->squery($product_insert_query);
                            if ($stmt) {

                                $stmt->execute()            or die ('Database error');
                                $stmt->store_result()       or die ('Database error');
                                if ($stmt->num_rows > 0) {
                                    $stmt->bind_result($brand)   or die ('Database error');
                                    while($stmt->fetch()) {
                                        echo "<option value='" . urldecode($brand) . "'>" .
                                            urldecode($brand) . "</option>";
                                    }
                                }
                                $stmt->close()              or die ('Database error');
                            }
                            ?>
                        </select>
                    </label>
                </div>
            </fieldset>
            <fieldset class='optional'>
                <legend>Optional</legend><hr>
                <div class='field-list'>
                    <label for='product-summary'>
                        <textarea name='product-summary'
                                  rows=6 cols=100
                                  placeholder="Product Summary"></textarea>
                    </label>
                </div>
                <div class='field-list'>
                    <label for='product-images[]'>
                        Product Images
                        <input type='file' name='product-images[]' multiple>
                        <input type='hidden' name='MAX_FILE_SIZE' value='4096000'> <!-- 500kb -->
                    </label>
                </div>
            </fieldset>

            <fieldset>
                <button name='product-add-submit'>Add product</button>
            </fieldset>
        </form>
        <?php
            break;

        default:
            throw new Exception('unknown action');
    }
}
?>