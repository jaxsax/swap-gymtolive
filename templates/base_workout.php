<div id='workout-content'>

    <p style='font-size:30px'>
        <b>Top 6th Workout</b>
    </p>
    <br>

    <b>Board Shoulder</b>
    <div class='image-container'>

        <div class='image-box'>

                <img src='http://localhost/gtl/static/img/board-shoulder.jpg' />
            <p style = 'text-align:justify; line-height:1.5'>
                The main shoulder muscle, the deltoid, is a three part muscle that goes around the shoulder joint.
                The anterior (front) deltoid starts on the collarbone, the medial (middle) deltoid starts
                on the top of the shoulder and the posterior (rear) deltoid starts on the shoulder blade.
            </p>

        </div>

        <br>

        <b>Horseshoes Triceps</b>
        <div class='image-box'>

                <img src='http://localhost/gtl/static/img/tricep_scr1.jpg' width=400; />

            <p style = 'text-align:justify; line-height:1.5'>
                Biceps may get most of the training attention but the triceps makes up two-thirds of your upper arm
                muscle mass. It's made up of three 'head'. Lateral, long and medial.
                The lateral and medial heads of the triceps are involved in straightening your arm,
                while the long head is engaged when you draws your arm down in front of you.
            </p>

        </div>

        <br>

        <b>Big Chest</b>
        <div class='image-box'>

            <img src='http://localhost/gtl/static/img/Chestscr2.jpg' width=400; />
            <p style = 'text-align:justify; line-height:1.5'>
                The main job of the chest muscles - the pectorals or 'pecs' - is to push your arms in front of you,
                although they are used when bring your arms down in front of you.
                There are two main muscles in this group.
                The pectoralis major is a large muscle that attaches to your collarbone, breastbone and ribs.
                The pectoralis minor is a thin triangular muscle in the upper part of the chest near the shoulder
                and below the pectoralis major. Its function is to bring the shoulder forward,
                an important part of generating strength in big chest moves such as bench press.
            </p>

        </div>

        <br>

        <b>Massive Biceps</b>
        <div class='image-box'>

            <img src='http://localhost/gtl/static/img/Biceps5.jpg' width=400; />
            <p style = 'text-align:justify; line-height:1.5'>
                Getting bigger biceps is a popular workout goal but they're actually a relatively small muscle.
                The biceps brachii is made up of two parts and is responsible for bending your arm.
                When your palms are facing down, the muscle that bends your arm is the brachialis.
                The final muscle in the group is the brachioradialis at the top of your forearm which is responsible for
                flexing the elbow.
            </p>

        </div>

        <br>

        <b>Muscular Back</b>
        <div class='image-box'>

            <img src='http://localhost/gtl/static/img/back.jpg' />
            <p style = 'text-align:justify; line-height:1.5'>
                Most men rather train their chest by doing bench presses than spend a session working on their back.
                But if you neglect the area you'll get bad posture and hunched shoulders as overdeveloped pecs draw
                your shoulder forwards. A strong back will help you lift big weights as well as giving you a broad and
                imposing physique.

            </p>

        </div>

        <br>

        <b>Hard Abs</b>
        <div class='image-box'>

            <img src='http://localhost/gtl/static/img/abs.jpg' />
            <p style = 'text-align:justify; line-height:1.5'>
                Your abdominals perform three main function (or four, if you count making you look better).
                They stabilise your body, keeping your trunk solid when external forces are acting on it.
                They're responsible for forward bending and rotational movements;
                and they control side bending and back extension movements.
                The four main muscles in the group are Transversus abdominis, Internal obliques,
                External obliques and Rectus abdominis.
            </p>

        </div>
    </div>
</div>