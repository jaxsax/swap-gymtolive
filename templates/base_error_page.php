<div id='error'>
    <?php
        require_once 'core/CommonFunctions.php';

        $eid = $_GET['e'];

        $error_id = \Common\Functions::retGETValue('e');
        $error_msg = \Common\Functions::getInfoTableValue('e_' . $error_id);
    ?>
    <h1>Unknown error <em><?php echo $error_id; ?></em> occured</h1>
    <p><?=$error_msg?></p>
</div>