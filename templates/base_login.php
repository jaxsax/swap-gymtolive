<?php

    require_once 'core/SessionHandler.php';
    require_once 'core/dbhandler.php';

    if (isset($_POST['_login_submit'])) {

        $username = $_POST['_username'];
        $password = $_POST['_pwd'];

        if (!\Session\SessionHandler::checkLogin($username, $password))
        {

            if(isset($_COOKIE['login'])) {
                if($_COOKIE['login'] < 3){
                    $attempts = $_COOKIE['login'] + 1;
                    setcookie('login', $attempts, time()+60*5); //set the cookie for 5 minutes with the number of attempts stored
                } else{
                    echo '<b>You have exceeded the maximum number of login attempts. Please try again later</b>';
                    exit;
                }

            } else{
                setcookie('login', 1, time()+60*5); //set the cookie for 5 minutes with the initial value of 1
            }

            echo '<b>Invalid username/password.<br><br></b>';

            exit;
        } else {
            /* Check for password expiry */
            $c = \Core\db::get()->connect();

            $s = \Core\db::get()->select(
                array('account_state'),
                'user',
                array('userid'),
                1
            );

            $s->bind_param('i', $_SESSION['uid']) or die;
            $s->execute() or die;
            $s->store_result() or die;
            if ($s->num_rows <= 0) {
                die('Invalid user');
            }

            $s->bind_result($state) or die;
            $s->fetch() or die;
            $user_state = $state;

            if ($state == 1) {
                /* Check if password has expired */
                $s->close();
                $query = "SELECT `userid` FROM `user` WHERE `last_expired` <= NOW() AND `userid` = ? LIMIT 1";
                $s = $c->prepare($query);
                if (!$s)
                    die('prepare() failed');
                $s->bind_param('s', $_SESSION['uid']) or die;
                $s->execute() or die;
                $s->store_result() or die;

                if ($s->num_rows > 0) {
                    /* User has expired password */
                    $s->close();

                    $update_query = "UPDATE `user` SET `account_state` = 0";
                    $s = $c->prepare($update_query);
                    if (!$s)
                        die('prepare() failed');
                    $s->execute() or die;
                    if ($s->affected_rows <= 0)
                        die('Failed to deactivate user');
                } else {
                    header('Location: ' . $_SERVER['HTTP_REFERER']);
                    exit;
                }
            } else if ($state == 0) {
                header('Location: ' . "activate.php?a=react");
                exit;
            }
        }
    }

    if (isset($_GET['s'])) {
        $a = $_GET['s'];
        if ($a == 'logout') {

            \Session\SessionHandler::destroy();
            header('Location: ' . "index.php");
            exit;
        }
    }

    if (isset($_GET['operation']) && isset($_SESSION['loggedin']))
    {
        $op = $_GET['operation'];
        switch($op) {
            case 'delete':

                $uid = $_SESSION['uid'];
                $con = \Core\db::get()->connect();

                $query = "UPDATE `user` SET `account_state` = 0 WHERE `userid` = ?";
                $stmt = $con->prepare($query);
                if (!$stmt)
                    die('prepare() failed');

                $stmt->bind_param('i', $uid) or die;
                $stmt->execute() or die;
                if ($stmt->affected_rows <= 0)
                    throw new Exception('account deactivation failed');

                \Session\SessionHandler::destroy();
                header('Location: ' . "index.php");
                exit;

                break;
        }
    }
    ?>
<div id='login'>
    <h1>
        <b>Member Login</b>
    </h1>

    <form action='login.php' method='post'>
        <table>
    <tr>
        <td><b>Username</b></td>
        <td>
            <label for='_username'>
                <input name='_username'>
            </label>
        </td>
    </tr>
    <tr>
        <td><b>Password</b></td>
        <td>
            <label for='_pwd'>
                <input name='_pwd' type='password'>
            </label>
        </td>
	</tr>
    <tr>
        <td>
            <label for='_login_submit'>
                <button name='_login_submit'>Login</button>
            </label>
        </td>
    </tr>

    <tr>
        <td><b>Not A Member Yet?</b></td>
    </tr>

    <tr>
        <td><b>
                Click <a href='register.php'>here</a> to register as a member!
        </b></td>
    </tr>

</table>
</form>
</div>