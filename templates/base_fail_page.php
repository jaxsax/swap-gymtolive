<!doctype html>
<html>
<head>
    <title>Profile modification error</title>
</head>
<body>
<?php
require_once 'core/CommonFunctions.php';
?>

<?php
if (!empty($_GET['error'])) {

    ?>
    <div id='fail'>
        <?php

        $error_id = $_GET['error'];

        if (empty($_GET['uid']))
            die('Invalid parameters specified');
        $uid = $_GET['uid'];

        switch($error_id) {
            case '1':
                ?>
                <h1>
                    <p>
                        Password should contain:
                    </p>
                    <br>
                    <p>
                        - At least minimum 8 character.
                    </p>
                    <br>
                    <p>
                        - Big capital letters.
                    </p>
                    <br>
                    <p>
                        - Small capital letters.
                    </p>
                    <br>
                    <p>
                        - Numerical values.
                    </p>

                </h1>
                <?php
                break;
            case '2':
                ?>
                <h1>
                    <b>Contact has to be 8 digits!</b>
                </h1>
                <?php
                break;
            case '3':
                ?>
                <h1>
                    <b>Invalid Address!</b>
                </h1>
                <?php
                break;
            case '4':
                ?>
                <h1>
                    <b>Invalid Postal Code!</b>
                </h1>
                <?php
                break;
            default:
                die('Invalid error id');
        }
        ?>
        <form action='manage_profile.php' method='get'>
            <table>
                <input type='hidden' name='uid' value='<?=$uid?>' />
                <button>Back</button>
            </table>
        </form>
    </div>
<?php
}
?>
</body>
</html>