<div id='cart-content'>

<?php

require_once 'core/SessionHandler.php';
require_once 'core/dbhandler.php';

/* recursive compare & swap transaction id */
function id_valid($pid) {

    if ($pid == null) {
        return id_valid(\Common\Security::generateRandChar(12));
    }
    $exists_query = "SELECT COUNT(*) FROM `transaction` WHERE `transaction_id` = ? LIMIT 1";
    $stmt = \Core\db::get()->squery($exists_query);
    if ($stmt) {
        $stmt->bind_param('s', $pid)    or die ('Database error');
        $stmt->execute()                or die ('Database error');
        $stmt->bind_result($count)      or die ('Database error');
        $stmt->fetch()                  or die ('Database error');
        $stmt->close()                  or die ('Database error');

        if ($count >= 1)
            return id_valid(\Common\Security::generateRandChar(12));
        return $pid;
    }
    die('Database error');
}

if (!isset($_SESSION['cart']))
    $_SESSION['cart'] = array();

if (isset($_POST['recaptcha-submit'])) {
    require_once 'core/recaptchalib.php';

    $privatekey = "6LeIveUSAAAAADq1lu19AW8fkPp1QOvfErSioP-T";
    $resp = recaptcha_check_answer ($privatekey,
        $_SERVER["REMOTE_ADDR"],
        $_POST["recaptcha_challenge_field"],
        $_POST["recaptcha_response_field"]);

    if (!$resp->is_valid) {
    ?>
        <div>
            <span>
                <b>reCAPTCHA was entered wrongly!</b>
            </span>
        </div>
        <br>
        <div>
            <span>
                <a href='cart.php'>
                    Back to Cart
                </a>
            </span>
        </div>
    <?php
        exit;

    } else {
        if (count($_SESSION['cart']) <= 0) {
            die('Really? Checking out an empty cart?');
        }

        $uid = $_SESSION['uid'];
        $tid = \Common\Security::generateRandChar(12);

        $c = \Core\db::get()->connect();

        $product_string = '';
        foreach($_SESSION['cart'] as $pid => $qty)
            $product_string .= "'" . $c->real_escape_string($pid) ."'" . ',';
        $product_string = rtrim($product_string, ", ");

        $product_query = "SELECT `product_id`, `unit_price` FROM `product`
        WHERE `product_id` IN (" . $product_string . ")";
        $s = $c->prepare($product_query);
        if (!$s)
            die('prepare() failed');

        $s->execute() or die;
        $s->store_result() or die;

        if ($s->num_rows <= 0) {
            die('hi');
        }

        $product_map = array();

        $s->bind_result($pid, $up) or die;
        while($s->fetch()) {
            $product_map[$pid] = $up;
        }

        $s->close();

        $transaction_query = "INSERT INTO `transaction`(`transaction_id`, `customer_id`) VALUES(?,?)";
        $transaction_item_query = "
        INSERT INTO `transaction_items`(`fk_transaction_id`, `fk_product_id`, `quantity`, `unit_price`)
        VALUES(?,?,?,?)";

        try {
            $c->autocommit(false);
            $s = $c->prepare($transaction_query);
            if (!$s)
                die('prepare() failed');

            $s->bind_param('si', $tid, $uid) or die;
            $s->execute() or die;
            if ($s->affected_rows <= 0)
                throw new Exception('unable to create transaction ' . $tid);
            $s->close();

            $s = $c->prepare($transaction_item_query);
            if (!$s)
                die('prepare() failed');

            foreach($_SESSION['cart'] as $pid => $qty) {
                $s->bind_param('ssid', $tid, $pid, $qty, $product_map[$pid]) or die;
                $s->execute() or die;

                if ($s->affected_rows <= 0) {
                    throw new Exception('Transaction item insert failed');
                }
            }
            $s->close();

            $c->commit();
            $c->autocommit(true);
        } catch (Exception $e) {
            $c->rollback();
        }

        $_SESSION['cart'] = array();

        die('Transaction Successful');
    }
}

if(!empty($_GET['operation']))
{
    $operation = $_GET['operation'];
    if ($operation == 'add') {
        if (empty($_GET['id'])) {
            die('unable to add unspecified id');
        }

        $pid = $_GET['id'];
        $_SESSION['cart'][$pid] += 1;

        if (isset($_SERVER['HTTP_REFERER'])) {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        } else {
            header('Location: ' . "cart.php");
        }
        exit;

    } else if ($operation == 'edit') {

        if (empty($_GET['id'])) {
            die('invalid parameters');
        }

        $pid = $_GET['id'];
        $s = \Core\db::get()->select(
            array('product_name'),
            'product',
            array('product_id'),
            1
        );

        $s->bind_param('s', $pid) or die;
        $s->execute() or die;
        $s->store_result() or die;
        if ($s->num_rows <= 0)
            die('invalid product id');
        $s->bind_result($pname) or die;
        $s->fetch() or die;

        $qty = isset($_SESSION['cart'][$pid]) ? $_SESSION['cart'][$pid] : 1;
        ?>
        <form method="post" action="cart.php"><table align="center" border="0">
            <table align='center'>
                <tr>
                    <td>
                        <b>Product name: <?=$pname?></b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input name='quantity' type='number' value='<?=$qty?>' />
                    </td>
                </tr>

                <input type='hidden' name='id' value='<?=$pid?>' />
                <tr>
                    <td align='center'>
                        <input type='submit' name='edit' value='Update Quantity' style='margin: 10px 0;'/>
                    </td>
                </tr>
            </table>
        </form>
    <?php
    } else if ($operation == 'remove') {
        if (empty($_GET['id'])) {
            die('unspecified product id');
        }

        $pid = $_GET['id'];
        unset($_SESSION['cart'][$pid]);

        header('Location: ' . "cart.php");
        exit;
    } else if ($operation == 'clear') {
        $_SESSION['cart'] = array();
        header('Location: ' . "cart.php");
        exit;
    } else {
        die('invalid action specified');
    }
}

if(isset($_POST['edit']))
{
    $product_id = $_POST['id'];
    $quantity = $_POST['quantity'];
    if (!is_numeric($quantity)) {
        die('invalid quantity, requires a number');
    } else if ($quantity <= 0) {
        unset($_SESSION['cart'][$product_id]);
    } else {
        $_SESSION['cart'][$product_id] = $quantity;
    }

    header('Location: ' . "cart.php");
    exit;
}
?>

<?php
if (count($_SESSION['cart']) <= 0) {
    ?>
    <span id='empty-cart-text'>
        Shopping Cart is Empty
        <br>
        <br>
        You have no item in your shopping cart.
        <br>
        Click <a href='product.php?page=1'>here</a> to continue shopping!
    </span>
<?php
} else {
?>
<table align='center' border='1'>
    <tr>
        <th><b>Product Name</b></th>
        <th><b>Product Price</b></th>
        <th><b>Product Type</b></th>
        <th><b>Product Brand</b></th>
        <th><b>Quantity</b></th>
        <th><b>Total price</b></th>
        <td colspan="2"><b>Update Shopping Cart</b></td>

    </tr>
    <?php

    $s = \Core\db::get()->select(
        array('product_name', 'unit_price','product_type','product_brand'),
        'product',
        array('product_id'),
        1
    );

    $total = 0.0;
    foreach($_SESSION['cart'] as $product_id => $quantity) {
        $s->bind_param('s', $product_id) or die;
        $s->execute() or die;
        $s->store_result() or die;
        if ($s->num_rows <= 0)
            die('retrieve: ' . $product_id . ' failed');
        $s->bind_result($product_name, $unit_price,$product_type,$product_brand) or die;
        $s->fetch() or die;

        ?>
        <tr>
            <td>
                <?=$product_name?>
            </td>
            <td>
                $<?=$unit_price?>
            </td>
            <td>
                <?=$product_type?>
            </td>
            <td>
                <?=$product_brand?>
            </td>
            <td align='center'>
                <?=$quantity?>
            </td>
            <td align='right'>
                $<?=number_format(($unit_price * $quantity), 2)?>
            </td>
            <td colspan="2">
                <a href='cart.php?operation=edit&id=<?=$product_id?>'>
                    Change Quantity
                </a>
                &nbsp
                <a href='cart.php?operation=remove&id=<?=$product_id?>'>
                    <img src='<?=\Common\Functions::linkAsset('img', 'DeleteRed.png')?>'
                        width=20 height=20 />
                </a>
            </td>

        </tr>
        <?php
        $total += ($unit_price * $quantity);
    }
    ?>
</table>
<div class='element-subtotal' align='center' style='margin-top: 10px;'>
    <span class='subtotal-left'>
        <b>Subtotal:</b>
    </span>
    <span class='subtotal-right'>
        $<?=number_format($total, 2)?>
    </span>
</div>
    <br>
<div class='bottom-panel' align='center'>
    <div class='cart-panel'>
        <a href='cart.php?operation=clear'>
            Empty cart
        </a>
    </div>
    <br>

    <div class='return-panel'>
        <a href='product.php?page=1'>
            Continue Shopping
        </a>
    </div>
    <br>
    <div class='recaptcha-panel'>

        <?php
        if (!isset($_SESSION['loggedin'])) {
            ?>
            <b>Unable to checkout without logging in</b>
            <?php
        } else {
            require_once 'core/recaptchalib.php';
        ?>

        <form action='cart.php' method='post'>
            <?=recaptcha_get_html('6LeIveUSAAAAAHwX__gDUxmQSas4Mm_1VpOl4c6Y')?>
            <br>
            <b><button name='recaptcha-submit' type='submit'>Proceed to Checkout</button></b>
        </form>

        <?php
        }
        ?>
    </div>
</div>
<?php
}
?>
</div>