</div>

<footer>
    <?php
    $end = microtime();
    ?>

    <div class='footer-element'>
        <h1>Page generated in <?=round(($end - $starttime), 4)?> seconds</h1>
    </div>
    <div class='footer-element'>
        <p style='text-align: center'>GymToLive &copy;</p>
    </div>
</footer>
</body>
</html>

