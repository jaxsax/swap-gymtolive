<?php
if (empty($_GET['uid'])) {
    die('Invalid user id');
}

$uid = $_GET['uid'];
?>
<div id='update'>
    <h1>
        <b>
            Update Successful
        </b>
    </h1>
    <form action='manage_profile.php' method='get'>
        <input type='hidden' name='uid' value='<?=$uid?>' />
        <button>Return to profile</button>
    </form>
</div>
