<div id='membership-content'>
    <div class="paragraph" style="text-align:justify; line-height:1.5">

        <p style='font-size:20px'>
            <b>Membership Privilege</b>
        <br>

        <ul>
            <li>
                Full year membership at only $35.
            </li>
        </ul>
        <ul>
            <li> Members get 10% discount for each supplement purchased.</li>
        </ul>
        <ul>
            <li> With membership, every $150 worth of supplements purchased, you will receive 15% discount, instead of
                just 10%
            </li>
        </ul>
        <ul>
            <li> It's your birthday!! - At your birthday month, our members get to receive $10 voucher and also 15% discount.
                instead of just 10% discount for the whole of birthday month!
            </li>
        </ul>
        <br>
        <?php /* TODO: implement code for sign up */?>
        <button type="button"><b>Sign Up Now</b></button>
    </div>
</div>