<?php
/* DYNAMIC LOGIC/CONTENT PAGE */

require_once 'core/html/HTMLInfo.php';
require_once 'core/html/HTMLTemplator.php';

\HTML\Info::setKey('TITLE', 'Products');

\HTML\Templator::getInstance()->add_asset('css', 'product', \Common\Functions::linkAsset('css', 'products.css'));
\HTML\Templator::getInstance()->add_asset('css', 'form-common', \Common\Functions::linkAsset('css', 'form-common.css'));

\HTML\Templator::getInstance()->add_template('templates/base_products.php');
\HTML\Templator::getInstance()->renderPage();