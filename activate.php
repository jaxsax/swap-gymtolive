<?php

$SECURE_PAGE = 1;
include_once 'core/https_config.php';

require_once 'core/SessionHandler.php';
\Session\SessionHandler::start('_GTL_LOGIN');
?>
<!doctype html>
<html>
<head>
    <title>Reactivation</title>

    <style>
        #content { width: 960px; margin: 0 auto; }
        #action-content .text-center { text-align: center; }
    </style>
</head>

<body>
<div id='content'>
    <div id='action-content'>
<?php

if (isset($_POST['password-react'])) {

    if (!isset($_SESSION['loggedin'])) {
        die('Not logged in');
    }
    $uid = $_SESSION['uid'];

    if (empty($_POST['current_password']) || empty($_POST['new_password']) || empty($_POST['new_password_retry'])) {
        ?>
        <span class='text-center'>Please fill in all fields</span>
        <?php
        exit;
    }

    $current_password = $_POST['current_password'];
    $new_password = $_POST['new_password'];
    $new_password_retry = $_POST['new_password_retry'];

    if (!preg_match('/^.*(?=.{8,})(?=.*[0-9])(?=.*[a-zA-Z]).*$/', $new_password)) {
        ?>
        <span class='text-center'>Please ensure your password is alphanumeric</span>
        <span class='text-center'>and at least 8 characters</span>
        <?php
        exit;
    }

    require_once 'core/CommonSecurity.php';
    $s = \Core\db::get()->select(
        array('salt', 'password'),
        'user',
        array('userid'),
        1
    );
    $s->bind_param('i', $uid) or die;
    $s->execute() or die;
    $s->store_result() or die;
    if ($s->num_rows <= 0) {
        ?>
        <span class='text-center'>Unknown user</span>
        <?php
        exit;
    }

    $s->bind_result($salt, $password) or die;
    $s->fetch() or die;

    $current_password_hashed = \Common\Security::generatePassword($current_password, $salt);
    if (!\Common\Security::slowEq($password, $current_password_hashed)) {
        ?>
        <span class='text-center'>Passwords did not match</span>
        <?php
        exit;
    }
    if (\Common\Security::slowEq($current_password, $new_password)) {
        ?>
        <span class='text-center'>Passwords can not be the same!</span>
        <?php
        exit;
    }

    if (!\Common\Security::slowEq($new_password, $new_password_retry)) {
        ?>
        <span class='text-center'>New password did not match with confirm password</span>
        <?php
        exit;
    }

    $new_password_salt = \Common\Security::generateRandChar();
    $new_password_hash = \Common\Security::generatePassword($new_password, $new_password_salt);

    $update_query = "UPDATE `user` SET `password` = ?, `salt` = ?,
    `last_expired` = DATE_ADD(NOW(), INTERVAL 90 DAY), `account_state` = 1
    WHERE `userid` = ?";
    require_once 'core/dbhandler.php';

    $c = \Core\db::get()->connect();
    $s = $c->prepare($update_query) or die;

    $s->bind_param('ssi', $new_password_hash, $new_password_salt, $uid) or die;
    $s->execute() or die;
    if ($s->affected_rows <= 0) {
        ?>
        <span class='text-center'>Unable to reactivate account</span>
        <?php
        exit;
    }

    header('Location: ' . "index.php");
    exit;
}

if (isset($_GET['a'])) {

    $action = $_GET['a'];
    switch($action) {
        case 'react':

            if (!isset($_SESSION['loggedin'])) {
                die('Not logged in');
            }

            $uid = $_SESSION['uid'];

            require_once 'core/dbhandler.php';
            $s = \Core\db::get()->select(
                array('user_name'),
                'user',
                array('userid'),
                1
            );
            $s->bind_param('i', $uid) or die;
            $s->execute() or die;
            $s->store_result() or die;
            if ($s->num_rows <= 0)
                die('Invalid user or account is already activated');

            $s->bind_result($user_name) or die;
            $s->fetch() or die;

            ?>
            <span class='text-center'>You will need to reactivate your account to continue using the website</span>
            <fieldset>
                <legend>
                    ACCOUNT REACTIVATION
                </legend>
                <form action='activate.php' method='post'>
                    <div>
                        <span>
                            Username: <?=$user_name?>
                        </span>
                    </div>
                    <div>
                        <label for='current_password'>
                            Current Password
                            <input name='current_password' input type='password' />
                        </label>
                    </div>
                    <div>
                        <label for='new_password'>
                            New password
                            <input name='new_password' input type='password' />
                        </label>
                    </div>
                    <div>
                        <label for='new_password_retry'>
                            New password reconfirm
                            <input name='new_password_retry' input type='password' />
                        </label>
                    </div>

                    <button name='password-react'>Reactivate</button>
                </form>
            </fieldset>
            <?php
            break;
    }
}

?>
    </div>
</div>
</body>
</html>