<?php
/* STATIC CONTENT PAGE */

require_once 'core/html/HTMLInfo.php';
require_once 'core/html/HTMLTemplator.php';

\HTML\Info::setKey('TITLE', 'Workouts');

\HTML\Templator::getInstance()->add_template('templates/base_workout.php');
\HTML\Templator::getInstance()->renderPage();

