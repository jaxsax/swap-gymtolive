<?php

$SECURE_PAGE = 1;
include_once 'core/https_config.php';
/* DYNAMIC LOGIC/CONTENT PAGE */

require_once 'core/html/HTMLInfo.php';
require_once 'core/html/HTMLTemplator.php';

\HTML\Info::setKey('TITLE', 'My cart');

\HTML\Templator::getInstance()->add_asset('css', 'cart', \Common\Functions::linkAsset('css', 'cart.css'));

\HTML\Templator::getInstance()->add_template('templates/base_cart.php');
\HTML\Templator::getInstance()->renderPage();