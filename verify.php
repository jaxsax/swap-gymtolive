<?php
/* [STATIC/DYNAMIC] LOGIC/CONTENT PAGE */

require_once 'core/html/HTMLInfo.php';
require_once 'core/html/HTMLTemplator.php';

\HTML\Info::setKey('TITLE', 'Setup your account');
\HTML\Templator::getInstance()->add_template('templates/base_verify.php');
\HTML\Templator::getInstance()->renderPage();
